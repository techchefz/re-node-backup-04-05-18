import express from 'express';
import validate from 'express-validation';
import passport from 'passport';
import APIError from '../helpers/APIError';
import config from '../../config/env';
import paramValidation from '../../config/param-validation';
import ctrl from '../controllers/recaptcha';

const router = express.Router();

// POST /api/recaptcha/store
router.route('/store')
    .post(ctrl.recaptchastore)
    .get(ctrl.recaptchastore);


router.route('/verify')
    .post(ctrl.recaptchaverify)
    .get(ctrl.recaptchaverify);

export default router;