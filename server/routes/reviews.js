import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import reviewsCtrl from '../controllers/reviews';


const router = express.Router();



// POST /api/reviews/create
router.route('/create')
  .post(reviewsCtrl.create)


// get /api/reviews/reviewsummary
router.route('/reviewsummary')
  .get(reviewsCtrl.getReviews)

//get /api/reviews/reviewdetails

router.route('/reviewdetails').
get(reviewsCtrl.fetch)



export default router;


