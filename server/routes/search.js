import express from 'express';
import solrCtrl from '../controllers/solr';

const router = express.Router();

router.route('/')
 .get(solrCtrl.searchSolr);

router.route('/searchride')
.post(solrCtrl.searchRide);

router.route('/testSms')
.post(solrCtrl.sendSms);

router.route('/tripStorySearch')
.post(solrCtrl.tripStorySearch);

router.route('/searchTripOnClick')
.post(solrCtrl.tripStorySearchOnClick);

export default router;
