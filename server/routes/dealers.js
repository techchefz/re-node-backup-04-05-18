import express from 'express';
import validate from 'express-validation';
import httpStatus from 'http-status';
import passport from 'passport';

import dealersCtrl from '../controllers/dealers';
import APIError from '../helpers/APIError';
import config from '../../config/env';
import paramValidation from '../../config/param-validation'


const router = express.Router();

/** POST /api/dealers/register - create new Dealer and return corresponding dealer object and token */
router.route('/register')
  .post(dealersCtrl.create);

router.route('/')
  /** GET /api/dealers - Get dealers */
  .get(dealersCtrl.get)

  /** PUT /api/dealers - Update dealers */
  .put(dealersCtrl.update)

  /** DELETE /api/bikes - Delete dealers */
  .delete(dealersCtrl.remove);

export default router;
