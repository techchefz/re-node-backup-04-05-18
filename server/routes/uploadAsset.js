import express from 'express';
import uploadCtrl from '../controllers/uploadAsset';
import path from 'path';
import CircularJSON from "circular-json-es6";

const router = express.Router();
router.route('/upload')
	.post(uploadCtrl.upload);

router.route('/fetch')
	.post(uploadCtrl.fetch);

export default router;
