import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import rideCtrl from '../controllers/ride';

const router = express.Router();

/** POST /api/rides/create - create new Ride and return corresponding ride object */
router.route('/create')
  .post(rideCtrl.create);

router.route('/all')
  .post(validate(paramValidation.getRides), rideCtrl.getRides);

router.route('/getridesaroundme')
  .post(rideCtrl.getRidesAroundMe);  

router.route('/')
  /** POST /api/rides/ - Get ride */
  .post(validate(paramValidation.getRide), rideCtrl.getRide)

  /** PUT /api/rides/ - Update ride */
  .put(rideCtrl.update)

  /** DELETE /api/rides/ - Delete ride */
  .delete(rideCtrl.remove);

export default router;
