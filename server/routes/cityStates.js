import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import cityStatesCtrl from '../controllers/cityStates';

const router = express.Router();


router.route('/getcity')
    .post(cityStatesCtrl.getcity);

router.route('/getstate')
    .post(cityStatesCtrl.getState);

router.route('/getdealers')
    .post(cityStatesCtrl.getDealers);

export default router;
