import express from 'express';
import validate from 'express-validation';
import passport from 'passport';
import APIError from '../helpers/APIError';
import config from '../../config/env';
import paramValidation from '../../config/param-validation';
import serviceBookingCtrl from '../controllers/serviceBooking';
import verifyOtpCtrl from '../controllers/verify';

import ctrl from '../controllers/serviceBooking';

const router = express.Router();

router.route('/getdealers')
    .post(ctrl.getDealers);


router.route('/findstate')
    .post(ctrl.findState);

router.route('/findcity')
    .post(ctrl.findCity);

router.route('/findpincode')
    .post(ctrl.findPincode);

router.route('/sendotp')
    .post(serviceBookingCtrl.sendVerificationSms);

router.route('/verify')
    .post(verifyOtpCtrl.verifyOtp);

router.route('/getBikeDetail')
    .post(ctrl.getBikeDetail);

router.route('/getSlots')
    .post(ctrl.getSlots);

router.route('/getDates')
    .post(ctrl.getDates);

router.route('/serviceBooking')
    .post(ctrl.serviceBooking);

    router.route('/getDate')
    .post(ctrl.getDates)
export default router;
