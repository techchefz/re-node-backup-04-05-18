import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import tripStoryCtrl from '../controllers/tripStory';

const router = express.Router();


router.route('/getthestory').post(tripStoryCtrl.getmystory);
/** POST /api/stories/create - create new Trip Story and return corresponding story object */
router.route('/create')
  .post(tripStoryCtrl.create);

router.route('/all')
  .post(validate(paramValidation.getStories), tripStoryCtrl.getStories);

router.route('/getstory')
  /** POST /api/stories/ - Get story */
  .post(tripStoryCtrl.getStory)
  .get(tripStoryCtrl.getStory)

  /** PUT /api/stories/ - Update Story */
  .put(tripStoryCtrl.update)

  /** DELETE /api/stories/ - Delete Story */
  .delete(tripStoryCtrl.remove);

export default router;
