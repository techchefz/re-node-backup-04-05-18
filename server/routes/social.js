
import express from 'express';
import validate from 'express-validation';
import passport from 'passport';
import APIError from '../helpers/APIError';
import config from '../../config/env';
import paramValidation from '../../config/param-validation';
import ctrlSocial from '../controllers/social';

const router = express.Router();

// POST /api/social/twitter
router.route('/twitter')
  .post(ctrlSocial.twitterdata)
  .get(ctrlSocial.twitterdata);

router.route('/reviews')
  .post(ctrlSocial.fetch);

router.route('/reviews')
  .get(ctrlSocial.fetch);

router.route('/googleplus')
  .post(ctrlSocial.googleplusdata);

router.route('/googleplus')
  .get(ctrlSocial.googleplusdata);

export default router;
