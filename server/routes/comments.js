import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import commentsCtrl from '../controllers/comments';

const router = express.Router();

/** POST /api/comments/create - create new Trip Story and return corresponding story object */
router.route('/create')
    .post(commentsCtrl.createComment);
router.route('/getComments').post(commentsCtrl.getComments);


export default router;
