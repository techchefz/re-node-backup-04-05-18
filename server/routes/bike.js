import express from 'express';
import validate from 'express-validation';
import httpStatus from 'http-status';
import passport from 'passport';

import APIError from '../helpers/APIError';
import config from '../../config/env';
import paramValidation from '../../config/param-validation';
import bikeCtrl from '../controllers/bike';

const router = express.Router();

/** POST /api/bikes/register - create new Bike and return corresponding bike object and token */
router.route('/register')
  .post(validate(paramValidation.createBike), bikeCtrl.create);


router.route('/')
  /** GET /api/bikes - Get bike */
  .get(bikeCtrl.get)

  /** PUT /api/bikes - Update bike */
  .put(bikeCtrl.update)

  /** DELETE /api/bikes - Delete bike */
  .delete(bikeCtrl.remove);

export default router;
