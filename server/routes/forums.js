import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import forumCtrl from '../controllers/forums';
import forumCategoryCtrl from "../controllers/forumCategory";

const router = express.Router();


router.route('/forum-detail')
  .post(forumCtrl.getFourmDetail);

router.route('/create-forum-post')
  .post(forumCtrl.create);

router.route('/increment-topic-view')
  .post(forumCtrl.incrementTopicViews);

router.route('/all')
  .post(forumCtrl.getForumPosts);

router.route('/create-category')
  .post(forumCategoryCtrl.create);

router.route('/category-details')
  .post(forumCategoryCtrl.getCategoryDetials);

  router.route('/subscribe')
  .post(forumCategoryCtrl.subscribeToForumCategory);
  
  
export default router;
