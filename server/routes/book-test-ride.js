import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import bookTestRideCtrl from '../controllers/book-test-ride';

const router = express.Router();

/** POST /api/comments/create - create new Trip Story and return corresponding story object */
router.route('/create')
    .post(bookTestRideCtrl.create);

export default router;