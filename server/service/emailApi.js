/* eslint-disable */
import nodemailer from 'nodemailer';
import smtpTransport from 'nodemailer-smtp-transport';
import path from 'path';
import UserSchema from '../models/user';


const EmailTemplate = require('email-templates').EmailTemplate;

const emailDir = path.resolve(__dirname, '../templates', 'emailVerify');
const emailVerify = new EmailTemplate(path.join(emailDir));

const registerDir = path.resolve(__dirname, '../templates', 'register');
const register = new EmailTemplate(path.join(registerDir));

function sendEmail(userId, responseObj, type) {
  UserSchema.findOneAsync({ _id: userId }).then((userObj) => {
    const details = {
      host: "smtp.mailtrap.io",
      port: 2525,
      username: "74b65bc36e217c",
      password: "453fbd4ef2ce51"
    }
    const transporter = nodemailer.createTransport(
      smtpTransport({
        host: details.host,
        port: details.port,
        secure: details.secure, // secure:true for port 465, secure:false for port 587
        auth: {
          user: details.username,
          pass: details.password
        }
      })
    );
    const locals = Object.assign({}, { data: responseObj });
    if (type === 'emailVerify') {
      emailVerify.render(locals, (err, results) => { //eslint-disable-line
        if (err) {
          return err; //eslint-disable-line
        }
        const mailOptions = {
          from: details.username, // sender address
          to: userObj.email, // list of receivers
          subject: 'Verify your Account with RoyalEnfield', // Subject line
          text: results.text, // plain text body
          html: results.html, // html body
        };
        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            return error;
          }
          return info;
        });
      });
    }

    if (type === 'register') {
      register.render(locals, (err, results) => { //eslint-disable-line
        if (err) {
          return err; //eslint-disable-line
        }
        const mailOptions = {
          from: details.username, // sender address
          to: userObj.email, // list of receivers
          subject: 'Your Account with RoyalEnfield is created', // Subject line
          text: results.text, // plain text body
          html: results.html // html body
        };
        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            return error;
          }
          return info;
        });
      });
    }

    if (type === 'forgot') {
      forgot.render(locals, (err, results) => {
        if (err) {
          return err;
        }
        const mailOptions = {
          from: details.username, // sender address
          to: userObj.email, // list of receivers
          subject: 'Your Account Password for RoyalEnfield', // Subject line
          text: results.text, // plain text body
          html: results.html // html body
        };
        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            return error;
          }
          return info;
        });
      });
    }
  });
}
export default sendEmail;
