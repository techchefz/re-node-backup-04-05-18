/* eslint-disable */
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai from 'chai';
import { expect } from 'chai';
import app from '../../index';

chai.config.includeStack = true;

describe('## Ride APIs', () => {
    let ride = {
        rideTitle: "Trip to Ladakh !",
        rideDescription: "Steep ride down the hills",
        rideStartDate: "1/1/2018",
        rideEndDate: "1/1/2018",
        totalDistance: "99",
        rideStartTime: "11:00am",
        rideOrigin: "New Delhi",
        rideDestination: "Ladakh",
        createrName: "Deepak Chourasiya",
        createrEmail: "deepu@chourasiya.com",
        createrPhoneNo: "1722149211",
        rideURL: "https://www.illeh.com/",
        createrId: "5a585bc036d5800618f3b67c",
        createdOn: "2014-08-31T18:30:00Z",
        rideType: "member-ride"
    };

    describe('# POST /api/rides/create', (done) => {
        it('should create a new ride', (done) => {
            request(app)
                .post('/api/rides/create')
                .send(ride)
                .expect(httpStatus.OK)
                .then((res) => {
                    expect(res.body.success).to.equal(true);
                    expect(res.body.data).to.have.all.keys('ride');
                    done();
                });
        })
    });

});
