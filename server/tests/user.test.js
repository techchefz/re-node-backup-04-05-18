/* eslint-disable */
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai from 'chai';
import { expect } from 'chai';
import app from '../../index';

chai.config.includeStack = true;

describe('## User(Public) APIs', () => {
  let user1 = {
    email: 'user1@techchefz.com',
    password: 'password',
    userType: 'user',
    fname: 'Kunal',
    lname: 'Bhardwaj',
    phoneNo: '9599914762'
  };
  
  let user2 = {
    email: 'user1@techchefz.com',
    password: 'password',
    userType: 'user',
    fname: 'Rahul',
    lname: 'Kumar',
    phoneNo: '9599914762'
  };

  let jwtAccessToken = null;
  describe('# POST /api/users/register', () => {
    it('should create a new user', (done) => {
      request(app)
        .post('/api/users/register')
        .send(user1)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          expect(res.body.data).to.have.all.keys('user', 'jwtAccessToken');
          user1 = res.body.data.user;
          jwtAccessToken = res.body.data.jwtAccessToken;
          done();
        });
    });
  });

  describe('# POST /api/users/register', () => {
    it('should not create new user with same email', (done) => {
      request(app)
        .post('/api/users/register')
        .send(user2)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(false);
          expect(res.body.message).to.equal('user already exist');
          done();
        });
    });
  });

  describe('# get /api/users', () => {
    it('should get the user details', (done) => {
      request(app)
        .get('/api/users')
        .set('Authorization', jwtAccessToken)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.success).to.equal(true);
          expect(res.body.data.fname).to.equal(user1.fname);
          done();
        });
    });
  });

  describe('# Error handling POST /api/users/register', () => {
    it('should throw parameter validation error', (done) => {
      delete user2.phoneNo;
      request(app)
        .post('/api/users/register')
        .send(user2)
        .expect(httpStatus.BAD_REQUEST)
        .then((res) => {
          expect(res.body.success).to.equal(false);
          done();
        });
    });
  });

  describe('# Error handling get /api/users', () => {
    it('should get UNAUTHORIZED error as no token provided', (done) => {
      request(app)
        .get('/api/users')
        .expect(httpStatus.UNAUTHORIZED)
        .then((res) => {
          expect(res.body.success).to.equal(false);
          done();
        });
    });
  });

});
