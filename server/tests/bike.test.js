/* eslint-disable */
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai from 'chai';
import { expect } from 'chai';
import app from '../../index';

chai.config.includeStack = true;

describe('## Bike APIs', () => {
    let bike = {
        bikeModel: 'Himalyan',
        bikeName: 'Himalyan',
        bikeCategory: 'Himalyan',
    };
    let bike2 = {
        bikeModel: 'Himalyan',
        bikeName: 'Himalyan',
        bikeCategory: 'Himalyan',
    };

    describe('# POST /api/bikes/register', (done) => {
        it('should create a new bike', (done) => {
            request(app)
                .post('/api/bikes/register')
                .send(bike)
                .expect(httpStatus.OK)
                .then((res) => {
                    expect(res.body.success).to.equal(true);
                    expect(res.body.data).to.have.all.keys('bike');
                    done();
                });
        })
    });

    describe('# POST /api/users/register', () => {
        it('should not create new bike with same bike name and category', (done) => {
            request(app)
                .post('/api/bikes/register')
                .send(bike2)
                .expect(httpStatus.OK)
                .then((res) => {
                    expect(res.body.success).to.equal(false);
                    expect(res.body.message).to.equal('bike already exist');
                    done();
                });
        });
    });
});
