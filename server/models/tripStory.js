import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import comments from './comments';
import replies from './replies';
import user from './user';

/**
 * Trip-Story Schema
 */

const Schema = mongoose.Schema;
const TripStorySchema = new mongoose.Schema({
    storyTitle: { type: String, default: null, required: true },
    storySummary: { type: String, default: null},
    storyBody: { type: String, default: null, required: true },
    storySummary: { type: String, default: null },
    categoryName: { type: String, default: null },
    postedOn: { type: Date, default: null },
    postedBy: { type: Schema.Types.ObjectId, ref: 'User', default: null },
    coverImage : {type : String,default : null},   
    storyUrl: { type: String, default: null },
    comment:[{type: mongoose.Schema.Types.ObjectId, ref:'comments',default : null}],
    tripStoryImages : [{
    srcPath : {type: String ,default : null}
  }],
  categoryName : {type : String,default : null}
});


/**
 * Statics
 */

TripStorySchema.statics = {
    getStories(skip, limit) {
        return this.find()
            .skip(skip)
            .limit(limit)
            .execAsync()
            .then((stories) => {
                if (stories) {
                    return stories;
                }
                const err = new APIError('Error Retrieving stories!', httpStatus.NOT_FOUND);
                return Promise.reject(err);
            });
    },

    getStory(id) {
        return this.find({ _id: id })
            .execAsync()
            .then((story) => {
                if (story) {
                    return story;
                }
                const err = new APIError('No Such story Exists !', httpStatus.NOT_FOUND);
                return Promise.reject(err);
            });
    }
};

export default mongoose.model('TripStory', TripStorySchema);
