import mongoose from 'mongoose';

var recaptchaSchema = new mongoose.Schema({
	Randomnumber: {
		type: Number,
		required: true,
		minlength: 1,
	},

	publickey:
		{
			type: String,
			require: true,
			minlength: 6
		},
	privatekey:
		{
			type: String,
			required: true,
			minlength: 1,
		}
});


export default mongoose.model('Recaptcha', recaptchaSchema);
