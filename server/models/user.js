import bcrypt from 'bcrypt';
import Promise from 'bluebird';
import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

/**
 * User Schema
 */

const Schema = mongoose.Schema;
const UserSchema = new mongoose.Schema({
  fname: { type: String, default: null },
  lname: { type: String, default: null },
  gender: { type: String, default: null },
  email: { type: String, required: true, unique: true },
  phoneNo: { type: String, default: null, unique: true },
  password: { type: String, required: true, select: false },
  bikename :{ type: String, default: null },
  city : {type:String,default : null},
  aboutMe: {type:String, default : null},
  profilePicture: {
    type: String,
    default:
      'http://localhost:4502/content/dam/re-platform-images/gt-profile-02.jpg' //Default profile image to be added.
  },
  coverImage: {
    type: String,
    default:
      'http://localhost:4502/content/dam/re-platform-images/banner-profile.jpg' //Default profile image to be added.
  },
  dob: { type: String, default: '12/8/1993' },
  addressInfo: {
    address: { type: String, default: null },
    city: { type: String, default: null },
    state: { type: String, default: null },
    country: { type: String, default: null },
  },
  ownBike: { type: String, default: null },
  bikeName: { type: String, default: null },
  mobileVerified: { type: Boolean, default: false },
  emailVerified: { type: Boolean, default: false },
  otp: { type: Number, default: null },
  emailToken: { type: Number, default: null },
  isRoyalEnfieldOwner: { type: String, default: false },
  bikeOwned: { type: Schema.Types.ObjectId, ref: 'Bike', default: null },
  ownedBikeInfo: {
    type: { type: String, default: null },
    regNo: { type: String, default: null },
    RC_ownerName: { type: String, default: null },
    vehicleNo: { type: String, default: null },
    bikeModel: { type: String, default: null },
    regDate: { type: Date, default: '1/1/2018' }
  },

  ridesCreated: [
    { type: Schema.Types.ObjectId, ref: 'Ride', default: null }
  ],

  tripStoriesCreated: [
    { type: Schema.Types.ObjectId, ref: 'TripStory', default: null }
  ],

  ridesJoined: [
    { type: Schema.Types.ObjectId, ref: 'Ride', default: null }
  ],

  favouriteQuote: {
    type: String, default: null
  },
  favouriteRideExperience: {
    type: String, default: null
  },
  socialNetworkUrls: {
    facebook: { type: String, default: null },
    googlePlus: { type: String, default: null }
  },
  userType: { type: String, default: 'user' },
  loginStatus: { type: Boolean, default: false },
  jwtAccessToken: { type: String, default: null },
  listofTags : [{
    userTags : {type: String ,default : null}
  }],
  userInterest: { type: String, default: null },
  userUrl : {type:String, default : null}
});


/**
 * converts the string value of the password to some hashed value
 * - pre-save hooks
 * - validations
 * - virtuals
 */
// eslint-disable-next-line
UserSchema.pre("save", function userSchemaPre(next) {
  const user = this;
  if (this.isModified('password') || this.isNew) {
    // eslint-disable-next-line
    bcrypt.genSalt(10, (err, salt) => {
      if (err) {
        return next(err);
      } // eslint-disable-next-line
      bcrypt.hash(user.password, salt, (hashErr, hash) => {
        //eslint-disable-line
        if (hashErr) {
          return next(hashErr);
        }
        user.password = hash;
        next();
      });
    });
  } else {
    return next();
  }
});

/**
 * comapare the stored hashed value of the password with the given value of the password
 * @param pw - password whose value has to be compare
 * @param cb - callback function
 */
UserSchema.methods.comparePassword = function comparePassword(pw, cb) {
  const that = this;
  // eslint-disable-next-line
  bcrypt.compare(pw, that.password, (err, isMatch) => {
    if (err) {
      return cb(err);
    }
    cb(null, isMatch);
  });
};
/**
 * Statics
 */
UserSchema.statics = {
  /**
     * Get user
     * @param {ObjectId} id - The objectId of user.
     * @returns {Promise<User, APIError>}
     */
  get(id) {
    return this.findById(id)
      .execAsync()
      .then((user) => {
        if (user) {
          return user;
        }
        const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
        return Promise.reject(err);
      });
  },
  /**
     * List users in descending order of 'createdAt' timestamp.
     * @param {number} skip - Number of users to be skipped.
     * @param {number} limit - Limit number of users to be returned.
     * @returns {Promise<User[]>}
     */
  list({ skip = 0, limit = 20 } = {}) {
    return this.find({ $or: [{ userType: 'user' }, { userType: 'admin' }] })
      .sort({ _id: -1 })
      .select('-__v')
      .skip(skip)
      .limit(limit)
      .execAsync();
  }
};
/**
 * @typedef User
 */
export default mongoose.model('User', UserSchema);
