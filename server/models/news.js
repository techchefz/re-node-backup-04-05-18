import mongoose from 'mongoose';

/**
 * News Schema
 */

const Schema = mongoose.Schema;
const NewsSchema = new mongoose.Schema({
    comment: [{ type: mongoose.Schema.Types.ObjectId, ref: 'comments', default: null }],
    newsUrl: { type: String, default: null }
});


export default mongoose.model('News', NewsSchema);
