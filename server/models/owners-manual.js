import mongoose from 'mongoose';

var ownersManualSchema = new mongoose.Schema({

name : {type:String, default: null},
email: {type: String, default: null},
phone: {type: String, default:null},
city: {type: String, default:null}

});

export default mongoose.model('OwnersManual',ownersManualSchema);