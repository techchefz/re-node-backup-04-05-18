import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';


/*** 
comments Schema 
***/
const moment = require('moment');
const Schema = mongoose.Schema;


const ReviewsSchema = new mongoose.Schema({


	reviewEntityId : {type:String , default : null},
	
	reviewByUser : {type: mongoose.Schema.Types.ObjectId, ref:'User',default : null},
	reviewDescription : {type : String , default : null},
	reviewText : {type : String , default : null},
	reviewDateText : {type:String , default : null},
	averageRating : {type:Number , default : 0},
	averageRatingPercentage : { type: Number,default : 0},
	reviewCriterias : {
		
			performance : {
				review : 
					{
				reviewCriteria : {type:String,default:null},
				reviewCriteriaLabel : {type : String,default : null},
				averageRating : {type: Number,default:0},
				averagePercentage : {type:Number,default:0}
					}
			},
			handling : 
			{
				review : 
						{
				reviewCriteria : {type:String,default:null},
				reviewCriteriaLabel : {type : String,default : null},
				averageRating : {type: Number,default:0},
				averagePercentage : {type:Number,default:0}
						}
			},
			style : {
					review : 
					{
				reviewCriteria : {type:String,default:null},
				reviewCriteriaLabel : {type : String,default : null},
				averageRating : {type: Number,default:0},
				averagePercentage : {type:Number,default:0}
						}	
			},
			verstality : 
			{    review : 
				{
				reviewCriteria : {type:String,default:null},
				reviewCriteriaLabel : {type : String,default : null},
				averageRating : {type: Number,default:0},
				averagePercentage : {type:Number,default:0}
						}
			},
			ownershipExperience : 
			{     review : {
				reviewCriteria : {type:String,default:null},
				reviewCriteriaLabel : {type : String,default : null},
				averageRating : {type: Number,default:0},
				averagePercentage : {type:Number,default:0}
						}
			}
		}
	




}); 




export default mongoose.model('Reviews', ReviewsSchema);