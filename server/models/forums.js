import mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import comments from './comments';
import replies from './replies';
import user from './user';

/**
 * Forums Schema
 */

const Schema = mongoose.Schema;

const ForumSchema = new mongoose.Schema({
    forumTitle: { type: String, default: null, required: true },
    forumBody: { type: String, default: null, required: true },
    categoryName: { type: String, default: null },
    postedOn: { type: Date, default: null },
    postedBy: { type: Schema.Types.ObjectId, ref: 'User', default: null },
    forumUrl: { type: String, default: null },
    views: { type:Number, default:0 },
    comment:[{type: mongoose.Schema.Types.ObjectId, ref:'comments',default : null}],
});


/**
 * Statics
 */

ForumSchema.statics = {
    getForumPosts(skip, limit) {
        return this.find()
            .skip(skip)
            .limit(limit)
            .execAsync()
            .then((stories) => {
                if (stories) {
                    return stories;
                }
                const err = new APIError('Error Retrieving Forums!', httpStatus.NOT_FOUND);
                return Promise.reject(err);
            });
    },

    getForumDetail(id) {
        return this.find({ _id: id })
            .execAsync()
            .then((story) => {
                if (story) {
                    return story;
                }
                const err = new APIError('No Such Forum Exists !', httpStatus.NOT_FOUND);
                return Promise.reject(err);
            });
    }
};

export default mongoose.model('Forum', ForumSchema);
