import mongoose from 'mongoose';

var dealerDetailsSchema = new mongoose.Schema({
    dealer_id: { type: Object, default: null },
    DealerName: { type: String, default: null },
    BranchName: { type: String, default: null },
    address: { 
    city: { type: String, default: null },
    State: { type: String, default: null },
    country: { type: String, default: null },
    pincode: { type: String, default: null },
    lat: {type: String, default: null},
    long: {type: String, default: null}
     },
    contact_number: { type: String, default: null },
    isServiceApplicable: { type: Boolean, default: false },
    isSalesApplicable: { type: Boolean, default: false },
    StartTime: { type: String, default: null },
    EndTime: { type: String, default: null },
    Week_Off: { type: Boolean, default: null },
    image: {type: String, default:null},
    email: {type: String, default:null},
    review: {
        reviewByUser: {
            firstName: {type: String, default:null},
            lastName: {type: String, default:null},
            profilePicture: {type: String, default:null}
        },
        reviewText: { type: String, default:null}
    },


});

export default mongoose.model('dealer', dealerDetailsSchema);