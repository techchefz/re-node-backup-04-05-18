import mongoose from 'mongoose';

/**
 * Bike Schema
 */

const Schema = mongoose.Schema;
const BikeSchema = new mongoose.Schema({
    bikeModel: { type: String, default: null },
    bikeName: { type: String, default: null },
    bikeCategory: { type: String, default: null }
});


export default mongoose.model('Bike', BikeSchema);
