import mongoose from 'mongoose';
import validator from "validator";

var UserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    trim: true,
    minlength: 1,
    unique: true,
    validate: {
      validator: validator.isEmail,
      message: '{VALUE} is not a valid email'
    }
  }
});

export default mongoose.model('Subscribe', UserSchema);

