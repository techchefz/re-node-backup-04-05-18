import mongoose from 'mongoose';

var Schema = mongoose.Schema;
const getCitiesSchema = new Schema({
    "CityId": { type: Number, default: null },
    "CityCode": { type: String, default: null },
    "CityName": { type: String, default: null },
    "StateId": { type: Number, default: null },
    "StateCode": { type: String, default: null },
    "StateName": { type: String, default: null },
    "Country": { type: String, default: null }
});

export default mongoose.model('cities', getCitiesSchema);