import mongoose from 'mongoose';

var twitterFeedSchema = new mongoose.Schema({
	User: {
		type: String,
		required: true,
		minlength: 1,
	},
	ProfileImage:
		{
			type: String,
			require: true,
			minlength: 2
		},
	Text:
		{
			type: String,
			required: true,
			minlength: 1,
		},
	Likes:
		{
			type: Number,
			required: true,

		},

	Retweets:
		{
			type: Number,
			required: true,

		}
});

export default mongoose.model('Twitterfeed', twitterFeedSchema); 