import mongoose from 'mongoose';

/**
 * Ride Schema
 */

const Schema = mongoose.Schema;
const RideSchema = new mongoose.Schema({
  ridePageUrl: { type: String, default: null },
  rideName: { type: String, default: null },
  startPoint: {
    latitude: { type: Number, default: 0 },
    longitude: { type: Number, default: 0 },
    name: { type: String, default: null }
  },
  endPoint: {
    latitude: { type: Number, default: 0 },
    longitude: { type: Number, default: 0 },
    name: { type: String, default: null }
  },
  waypoints: [{
    latitude: { type: Number, default: 0 },
    longitude: { type: Number, default: 0 },
    name: { type: String, default: null }
  }],
  durationInHours: { type: Number, default: null },
  calculatedDistance: { type: Number, default: 0 },
  durationInDays: { type: Number, default: null },
  rideDetails: { type: String, default: null },
  startDate: { type: String, default: null },
  endDate: { type: String, default: null },
  terrain: { type: String, default: null },
  createdByUser: { type: Schema.Types.ObjectId, ref: 'user' },
  ridersJoined: [{ type: Schema.Types.ObjectId, default: null }],
  startTime: { type: String, default: null },
  totalDistance: { type: Number, default: null },
  rideImages: [{
    srcPath: { type: String, default: null }
  }],
  rideCategory: { type: String, default: null },
  personalInfo: {
    fName: { type: String, default: null },
    lName: { type: String, default: null },
    gender: { type: String, default: null },
    email: { type: String, default: null },
    password: { type: String, default: null },
    isRoyalEnfieldOwner: { type: Boolean, default: null },
    dob: { type: String, default: null },
    city: { type: String, default: null },
    bikeName: { type: String, default: null }
  }
});

export default mongoose.model('Ride', RideSchema);
