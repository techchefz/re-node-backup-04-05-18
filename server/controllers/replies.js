import Comments from '../models/comments';
import Replies from '../models/replies';
import mongoose from 'mongoose';

function createReplies(req, res, next) {
    var id = req.body.commentid;
    var userid = req.body.userid;
    var replyBody = req.body.replyBody;
	console.log("-------------------------------------------------------------------------------------------------------------");
	console.log(req.body);
    Comments.find({ _id: id }).then((commentsarray) => {
        var reply = new Replies({
            replyBody: replyBody,
            userdetailsreplies: userid
        });

        reply.save().
            then((doc) => {
                commentsarray[0].Replyid.push(doc._id);
                commentsarray[0].save().then((details) => {
                    res.send("replysaved");
                });
            });
    });
}

export default { createReplies };
