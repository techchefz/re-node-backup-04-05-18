import ForumCategory from '../models/forumCategory';

function create(req, res, next) {
    const forumCategory = new ForumCategory({
        categoryName: req.body.categoryName,
        categorySubHeading: req.body.categorySubHeading,
    })
    forumCategory
    .save()
    .then((savedCategory) => {
        res.send("Category Created");
    }, (err) => {
        console.log(e);
        res.send("Error Creating Category");
    })
}

function getCategoryDetials(req, res, next) {
    ForumCategory.findOne({ categoryName: req.body.categoryName }).then((categoryDetails) => {
        var returnObj = {
            categoryName: categoryDetails.categoryName,
            categorySubHeading: categoryDetails.categorySubHeading,
            subscribersCount: categoryDetails.subscribedUsers.length ? categoryDetails.subscribedUsers.length : 0,
            repliesCount: categoryDetails.totalRepliesCount ? categoryDetails.totalRepliesCount : 0,
            viewsCount: categoryDetails.totalViewsCount ? categoryDetails.totalViewsCount : 0,
            topicsCount: categoryDetails.totalRepliesCount ? categoryDetails.totalRepliesCount : 0
        }
        res.send(returnObj);
    },(err) => {
        res.send(err);
    })
}

function subscribeToForumCategory(req, res, next) {
    ForumCategory.findOneAndUpdate({ categoryName: req.body.categoryName },{ $push:{ subscribedUsers: { userId: req.body.userId, email: req.body.email }}})
    .then((updatedForumCategory) => {
        res.send("User Subscribed Successfully");
    }, (err) => {
        res.send("Error Subscribing");
    })
}

export default {
    create,
    getCategoryDetials,
    subscribeToForumCategory,
}