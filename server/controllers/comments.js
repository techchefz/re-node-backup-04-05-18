import Comments from '../models/comments';
import News from '../models/news';
import Forums from '../models/forums';
import ForumCategory from "../models/forumCategory";
import TripStory from '../models/tripStory';
import mongoose from 'mongoose';

function incrementCategoryRepliesCounter(topicCategoryName) {
    ForumCategory.findOneAndUpdate({categoryName: topicCategoryName },  { $inc : { "totalRepliesCount" : 1 } }).then((doc) => {
        return true;
    }, (err) => {
        console.log(err);
    })
}

function createComment(req, res, next) {
    var id = req.body.pageid;
    var userid = req.body.userid;
    var commentBody = req.body.commentBody;
    var category =  req.body.category;
    var forumCategory = req.body.forumCategory ? req.body.forumCategory : null;
console.log("------------------------------------------------xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx----------------------------------------------");
	console.log(req.body);

    if (category === 'TripStory') {
        TripStory.findOne({ _id: id }).then((tripstory) => {
            var comment = new Comments({
                commentBody: commentBody,
                userdetailscomments: userid
            });
            comment.save()
                .then((doc) => {                
                    tripstory.comment.push(doc._id);
                    tripstory.save().then((details) => {
                        res.send("comments saved");
                    }, (err)=>{
                        console.log(err)
                    });
                });
        });
    } else if(category === 'News') {
        News.findOne({ _id: id }).then((news) => {
            var comment = new Comments({
                commentBody: commentBody,
                userdetailscomments: userid
            });
            comment.save()
                .then((doc) => {                
                    news.comment.push(doc._id);
                    news.save().then((details) => {
                        res.send("comments saved");
                    }, (err)=>{
                        console.log(err)
                    });
                });
        });  
    } else if(category === 'Forums') {
        Forums.findOne({ _id: id }).then((forumPost) => {
            var comment = new Comments({
                commentBody: commentBody,
                userdetailscomments: userid
            });
            comment.save()
                .then((doc) => {
                    incrementCategoryRepliesCounter(forumCategory);                
                    forumPost.comment.push(doc._id);
                    forumPost.save().then((details) => {
                        res.send("comments saved");
                    }, (err)=>{
                        console.log(err)
                    });
                });
        });
    }

}


function getComments(req,res,next)
{
    var id = req.body.pageid;
    var category =  req.body.category;
console.log(req.body);   
  if (category === 'trip-story' || category === 'TripStory') 
     {	console.log("inside trip");
         TripStory.find({ _id: id }).populate(
            {
            path: 'comment',
            model: 'comments',
            populate: {
            path: 'Replyid userdetailscomments',
            populate: { path: 'userdetailsreplies' }
            },
            }).then((tripstory) => {
console.log(tripstory[0]);
            res.send(tripstory[0]);

                });
    }
    else if(category === 'News')
    {
          News.find({ _id: id }).populate(
            {
            path: 'comment',
            model: 'comments',
            populate: {
            path: 'Replyid userdetailscomments',
            populate: { path: 'userdetailsreplies' }
            },
            }).then((news) => {
            res.send(news[0]);
                });
    }
    else if(category === 'Forums')
    {
        Forums.find({ _id: id }).populate(
            {
            path: 'comment',
            model: 'comments',
            populate: {
            path: 'Replyid userdetailscomments',
            populate: { path: 'userdetailsreplies' }
            },
            }).then((forums) => {
            res.send(forums[0]);
                });
    }

}



export default {
    createComment,
	getComments,
};



