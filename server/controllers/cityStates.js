import CityStates from '../models/citiesStates';
import Dealers from '../models/dealers';

var headers = {
    'Content-Type': 'text/plain'
};

var fetchCityStatesData = {
    "ResourceIdentifier": 206,
    "EntityType": "9021",
    "EntityID": 45,
    "CardType": 34,
    RequestKey: "kjwkdjsddcawdwhcb",
}

var fetchCityStatesString = JSON.stringify(fetchCityStatesData);

var fetchCityStatesOptions = {
    url: 'http://refineb2cdvp.excelloncloud.com/exapis//api/portalentity',
    method: 'POST',
    headers: headers,
    body: fetchCityStatesString
}

function fetchCityStates(req, res, next) {

    request(fetchCityStatesOptions, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            // Print out the response body
            console.log(body);
            res.send(body);
            const cityStatesData = JSON.parse(body);
            CityStates.insertMany(cityStatesData, (doc) => {
                res.send(doc);
            });
        } else {
            console.log(error);
        }
    });

}

function getcity(req, res, next) {
    CityStates.find({ StateName: req.body.stateName }).then((doc) => {
        let citiesArr = [];
        doc.map(a => {
            citiesArr.push(a.CityName)
        });
        res.send(citiesArr);
    });

}

function getState(req, res, next) {
    CityStates.distinct('StateCode').then((doc) => {
        res.send(doc);
        console.log(doc);
    });

}

function getDealers(req, res, next) {
    
    Dealers.find({ city: req.body.city }).then((doc) => {
        var dealersArr = [];
        doc.map(dealer => {
            let dealerObj = {
                "DealerName": dealer.DealerName,
                "BranchName": dealer.BranchName,
                "address": dealer.address,
                "BranchID": dealer.BranchID
            }
            dealersArr.push(dealerObj);
        })
        res.send(dealersArr);
    });

   

}
export default {
    getState,
    getcity,
    getDealers
}


