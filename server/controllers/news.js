
import News from '../models/news';

function getNews(req, res, next) {
    var id = req.body.newsUrl;

    News.findOne({ _id: id }).populate(
        {
            path: 'comment',
            model: 'comments',
            populate: {
                path: 'Replyid userdetailscomments',
                populate: { path: 'userdetailsreplies' }
            },
        }).then((news) => {
            res.send(news);
        });
}

function create(req, res, next) {
    const news = new News({
        newsUrl: req.body.newsUrl
    });
    news
        .saveAsync()
        .then((savedNews) => {
            res.send(savedNews._id);
        })
        .error(e => next(e));
}


function update (req, res, next){
    News.findOne({_id: req.body._id}).then((doc)=>{
        doc.comment = req.body.comment ? req.body.comment : doc.comment
    })
    News    
        .saveAsync()
        .then((updatedNews)=>{
            res.send(updatedNews+"updated Successfully");
        })
}

export default {
    create,
    getNews,
    update
}