import Recaptcha from '../models/recaptcha';
import request from 'request';


function recaptchastore(req, res, next) {
  req.on("data", function (buffer) {
    var obj = JSON.parse(buffer);
    var randomnumber = obj.randomnumber;
    var publickey = obj.publickey;
    var privatekey = obj.privatekey;

    var recaptcha = new Recaptcha({
      Randomnumber: randomnumber,
      publickey: publickey,
      privatekey: privatekey
    });

    recaptcha.save().then((doc) => {
      console.log(doc);
    }, (e) => {
      console.log(e);
    });

    res.send('asdasdsa');
  });
}

function recaptchaverify(req, res, next) {
  if (
    req.body.captcha === undefined ||
    req.body.captcha === '' ||
    req.body.captcha === null
  ) {
    return res.json({ "success": false, "msg": "Please select captcha" });
  }

  var randomnumber = req.body.randomnumber;
  var publickey = req.body.publickey;

  Recaptcha.find({ randomnumber: randomnumber }).then((todos) => {
    console.log(todos);
  }, (e) => {
    console.log(e);
  });

  //Secret Key
  const secretKey = '6Le9V0QUAAAAADlrF3TzTyVy8mPhmd-vgOqN5RyL';

  // Verify URL
  const verifyUrl = `https://google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${req.body.captcha}&remoteip=${req.connection.remoteAddress}`;

  // Make Request To VerifyURL
  request(verifyUrl, (err, response, body) => {
    body = JSON.parse(body);
    console.log(body);

    // If Not Successful
    if (body.success !== undefined && !body.success) {
      console.log('invalid private key');
    }
  });
}

export default {

  recaptchaverify,
  recaptchastore

};