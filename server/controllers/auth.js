import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import APIError from '../helpers/APIError';
import config from '../../config/env';
import UserSchema from '../models/user';
const crypto = require('crypto');
const { OAuth2Client } = require('google-auth-library');
var google = require('googleapis');
var request = require('request');

/**
 * Returns jwt token  and user object if valid email and password is provided
 * @param req (email, password, userType)
 * @param res
 * @param next
 * @returns {jwtAccessToken, user}
 */
function login(req, res, next) {
  var retObj = {
    success: true,
    message: '',
    data: {},
  };

  const userObj = {
    email: req.body.email,
    userType: req.body.userType,
  };

  UserSchema.findOneAsync(userObj, '+password')
    .then((user) => {
      //eslint-disable-line
      if (!user) {
        const err = new APIError('User not found with the given email id', httpStatus.NOT_FOUND);
        return next(err);
      } else {
        user.comparePassword(req.body.password, (passwordError, isMatch) => {
          //eslint-disable-line
          if (passwordError || !isMatch) {
            retObj.success = false;
            retObj.message = 'Incorrect Password';
            res.json(retObj);
          }
          user.loginStatus = true;
          const token = jwt.sign(user, config.jwtSecret);
          UserSchema.findOneAndUpdateAsync({ _id: user._id }, { $set: user }, { new: true }) //eslint-disable-line
            .then((updatedUser) => {
              var userObj = { email: updatedUser.email, profilePicture: { srcPath: updatedUser.profilePicture }, userId: updatedUser._id, phone: updatedUser.phoneNo, firstName: updatedUser.fname, lastName: updatedUser.lname };              
              retObj.success = true;
              retObj.message = 'user successfully logged in';
              retObj.data.jwtAccessToken = `JWT ${token}`;
              retObj.data.user = userObj;
              res.json(retObj);
            })
            .error((err123) => {
              const err = new APIError(`error in updating user details while login ${err123}`, httpStatus.INTERNAL_SERVER_ERROR);
              next(err);
            });
        });
      }
    })
    .error((e) => {
      const err = new APIError(`erro while finding user ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
      next(err);
    });
}

/** This is a protected route. Change login status to false and send success message.
 * @param req
 * @param res
 * @param next
 * @returns success message
 */

function logout(req, res, next) {
  const userObj = req.user;
  if (userObj === undefined || userObj === null) {
    console.log('user obj is null or undefined inside logout function'); //eslint-disable-line
  }
  userObj.loginStatus = false;
  UserSchema.findOneAndUpdate({ _id: userObj._id, loginStatus: true }, { $set: userObj }, { new: true }, (err, userDoc) => {
    //eslint-disable-line
    if (err) {
      const error = new APIError('error while updateing login status', httpStatus.INTERNAL_SERVER_ERROR);
      next(error);
    }
    if (userDoc) {
      const returnObj = {
        success: true,
        message: 'user logout successfully',
      };
      res.json(returnObj);
    } else {
      const error = new APIError('user not found', httpStatus.NOT_FOUND);
      next(error);
    }
  });
}

function checkUser(req, res) {
  UserSchema.findOneAsync({ email: req.body.email })
    .then((foundUser) => {
      if (foundUser !== null) {
        const jwtAccessToken = jwt.sign(foundUser, config.jwtSecret);
        const returnObj = {
          success: true,
          message: 'User Exist',
          data: {},
        };
        returnObj.data = {
          user: foundUser,
          jwtAccessToken: `JWT ${jwtAccessToken}`,
        };
        return res.send(returnObj);
      } else {
        const returnObj = {
          success: true,
          message: 'New User',
        };
        return res.send(returnObj);
      }
    })
    .catch((error) => {
    });
}



function facebookSignup(req, res, next) {
  var retObj = {
    status: false,
    userObj: null,
    message: null
  };

  var accessToken = req.body.AccessToken;
  var clientSecret = '7a73a87525b5af1c809058c73f42dca1';
  var appsecret_proof = crypto.createHmac('sha256', clientSecret).update(accessToken).digest('hex');
  var verifyUrl = `https://graph.facebook.com/v2.12/me?fields=id,email,first_name,last_name,picture,gender&access_token=${accessToken}&appsecret_proof=${appsecret_proof}`;
  request(verifyUrl, (err, response, body) => {
    var responseBody = JSON.parse(body);
    UserSchema.findOne({ email: responseBody.email }) //eslint-disable-line
      // eslint-disable-next-line
      .then((UserObj) => {
        if (UserObj == null) {
          retObj.status = true,
            retObj.userObj = responseBody,
            retObj.message = null;
          res.send(retObj);
        } else if (UserObj.email === responseBody.email) {
          retObj.status = false;
          retObj.userObj = null;
          retObj.message = "User Already Exists !";
          res.send(retObj);
        } else {
          retObj.status = false;
          retObj.userObj = null;
          retObj.message = "Error Creating User, Please Try again !";
        }
      }, (err) => {
        console.log(err);
      })
  });
}


function googleSignup(req, res, next) {
  var retObj = {
    status: false,
    userObj: null,
    message: null
  };

  const ClientId = "800698914082-gshvmfmti62vt5ltm49ibgn7g2s43rkb.apps.googleusercontent.com";
  const ClientSecret = "Ux4kQpJp96pyxObSSaa6XK_2";

  function getOAuthClient() {
    return new OAuth2Client(ClientId, ClientSecret, "http://localhost:4502");
  }

  var oauth2Client = getOAuthClient();
  var code = req.body.authCode;
  oauth2Client.getToken(code, function (err, tokens) {
    // Now tokens contains an access_token and an optional refresh_token. Save them.
    if (!err) {
      oauth2Client.setCredentials(tokens);
      var token = tokens.id_token;
      var verifyUrl = `https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=${token}`;
      request(verifyUrl, (err, response, body) => {
        var responseBody = JSON.parse(body);
        UserSchema.findOne({ email: responseBody.email }) //eslint-disable-line
          // eslint-disable-next-line
          .then((UserObj) => {
            if (UserObj == null) {
              retObj.status = true,
                retObj.userObj = responseBody,
                retObj.message = null;
              res.send(retObj);
            } else if (UserObj.email === responseBody.email) {
              retObj.status = false;
              retObj.userObj = null;
              retObj.message = "User Already Exists !";
              res.send(retObj);
            } else {
              retObj.status = false;
              retObj.userObj = null;
              retObj.message = "Error Creating User, Please Try again !";
            }
          }, (err) => {
            console.log(err);
          })
      });
    }
    else {
      console.log(err);
    }
  });
}

function facebookLogin(req, res, next) {
  var retObj = {
    status: true,
    message: '',
    data: {},
  };
  var accessToken = req.body.obj.accessToken;
  var clientSecret = '7a73a87525b5af1c809058c73f42dca1';
  var appsecret_proof = crypto.createHmac('sha256', clientSecret).update(accessToken).digest('hex');
  var verifyUrl = `https://graph.facebook.com/v2.12/me?fields=id,email,first_name,last_name,picture,gender&access_token=${accessToken}&appsecret_proof=${appsecret_proof}`;
  request(verifyUrl, (err, response, body) => {
    var responseBody = JSON.parse(body);        
    UserSchema.findOne({ email: responseBody.email }) //eslint-disable-line
      // eslint-disable-next-line
      .then((UserObj) => {
        if (UserObj == null) {
          retObj.status = false;
          retObj.user = null;
          retObj.message = "User Doesn't Exists !";
          res.send(retObj);
        } else if (UserObj.email === responseBody.email) {
          retObj.status = true;
          retObj.message = "Logged in Successfully";
          const jwtAccessToken = jwt.sign(UserObj, config.jwtSecret);
          retObj.data.jwtAccessToken = `JWT ${jwtAccessToken}`;
          var obj = { email: UserObj.email, profilePicture: { srcPath: UserObj.profilePicture }, userId: UserObj._id, phone: UserObj.phoneNo, firstName: UserObj.fname, lastName: UserObj.lname };
          retObj.data.user = obj;
          res.send(retObj);
        } else {
          retObj.status = false;
          retObj.user = null;
          retObj.message = "Error !, Please Try again !";
          res.send(retObj);
        }
      }, (err) => {
        console.log(err);
      })
  });
}


function googleLogin(req, res, next) {
  var retObj = {
    success: true,
    message: '',
    data: {},
  };

  const ClientId = "800698914082-gshvmfmti62vt5ltm49ibgn7g2s43rkb.apps.googleusercontent.com";
  const ClientSecret = "Ux4kQpJp96pyxObSSaa6XK_2";

  function getOAuthClient() {
    return new OAuth2Client(ClientId, ClientSecret, "http://localhost:4502");
  }

  var oauth2Client = getOAuthClient();
  var code = req.body.obj.authCode;
  oauth2Client.getToken(code, function (err, tokens) {
    // Now tokens contains an access_token and an optional refresh_token. Save them.
    if (!err) {
      oauth2Client.setCredentials(tokens);
      var token = tokens.id_token;
      var verifyUrl = `https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=${token}`;
      request(verifyUrl, (err, response, body) => {
        var responseBody = JSON.parse(body);        
        UserSchema.findOne({ email: responseBody.email }) //eslint-disable-line
          // eslint-disable-next-line
          .then((UserObj) => {
            if (UserObj == null) {
              retObj.status = false;
              retObj.user = null;
              retObj.message = "User Doesn't Exists !";
              res.send(retObj);
            } else if (UserObj.email === responseBody.email) {
              retObj.status = true;
              retObj.message = "Logged in Successfully";
              const jwtAccessToken = jwt.sign(UserObj, config.jwtSecret);
              retObj.data.jwtAccessToken = `JWT ${jwtAccessToken}`;
              var obj = { email: UserObj.email, profilePicture: { srcPath: UserObj.profilePicture }, userId: UserObj._id, phone: UserObj.phoneNo, firstName: UserObj.fname, lastName: UserObj.lname };
              retObj.data.user = obj;
              res.send(retObj);
            } else {
              retObj.status = false;
              retObj.user = null;
              retObj.message = "Error !, Please Try again !";
              res.send(retObj);
            }
          }, (err) => {
            console.log(err);
          })
        });
    }
    else {
      console.log(err);
    }
  });
}


export default { login, logout, checkUser, facebookSignup, googleSignup, googleLogin, facebookLogin };

