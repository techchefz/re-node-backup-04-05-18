/* eslint-disable */
import User from '../models/user';

function mobileVerify(req, res, next) {
  User.findOneAsync({ email: req.query.email })
    //eslint-disable-next-line
    .then(foundUser => {
      if (foundUser) {
        const host = req.get('host');
        if ((req.protocol + "://" + req.get('host')) == ("http://" + host)) {
          if (req.query.otp == foundUser.otp) {
            User.findOneAndUpdateAsync({ email: req.query.email }, { $set: { mobileVerified: true } }, { new: true }) //eslint-disable-line
              .then((updateUserObj) => { //eslint-disable-line
                if (updateUserObj) {
                  const returnObj = {
                    success: false,
                    message: 'Mobile verified',
                    data: {}
                  };
                  returnObj.success = true;
                  return res.send(returnObj);
                }
              })
              .error((e) => {
                const err = new APIError(`error in updating user details while login ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
                next(err);
              });
          }
          else {
            const returnObj = {
              success: false,
              message: 'Error Verifying Mobile',
              data: {}
            };
            return res.send(returnObj);
          }
        }
      }
    });
}

function emailVerify(req, res, next) {
  User.findOneAsync({ email: req.query.email })
    //eslint-disable-next-line
    .then(foundUser => {
      if (foundUser) {
        const host = req.get('host');
        if ((req.protocol + "://" + req.get('host')) == ("http://" + host)) {
          if (req.query.verificationToken == foundUser.emailToken) {
            User.findOneAndUpdateAsync({ email: req.query.email }, { $set: { emailVerified: true } }, { new: true }) //eslint-disable-line
              .then((updateUserObj) => { //eslint-disable-line
                if (updateUserObj) {
                  const returnObj = {
                    success: false,
                    message: 'Email verified',
                    data: {}
                  };
                  returnObj.success = true;
                  return res.send(returnObj);
                }
              })
              .error((e) => {
                const err = new APIError(`error in updating user details while login ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
                next(err);
              });
          }
          else {
            const returnObj = {
              success: false,
              message: 'Error Verifying Email',
              data: {}
            };
            return res.send(returnObj);
          }
        }
      }
    });
}

function verifyOtp(req, res, next) {
  var phoneNo = req.body.phoneNo;
  User.find({ phoneNo: phoneNo }).then((user) => {
    var otp = req.body.otp;
    console.log(otp);
    if (user[0].otp == otp) {
      res.send("Otp verified");

    }
    else {
      res.send("Otp wrong");
    }
  }).catch((e) => {
    console.log(e);
  });
}

export default { emailVerify, mobileVerify, verifyOtp };
