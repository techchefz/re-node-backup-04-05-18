import bcrypt from 'bcrypt';
import httpStatus from 'http-status';
import formidable from 'formidable';
import jwt from 'jsonwebtoken';
import APIError from '../helpers/APIError';
import config from '../../config/env';
import User from '../models/user';
import sendEmail from '../service/emailApi';
import SendSms from '../service/smsApi';
import axios from 'axios';
import request from 'request';
/**
 * Get user
 * @returns {User}
 */

function get(req, res) {
  return res.send({ success: true, message: 'user found', data: req.user });
}

/**
 * Create new user
 * @property {string} req.body.username - The username of user.
 * @property {string} req.body.mobileNumber - The mobileNumber of user.
 * @returns {User}
 */

function create(req, res, next) {
  User.findOneAsync({
    $or: [{ $and: [{ email: req.body.email }, { phoneNo: req.body.phoneNo }] }, { $or: [{ email: req.body.email }, { phoneNo: req.body.phoneNo }] }],
  }).then((foundUser) => {
    if (foundUser !== null && foundUser.userType === req.body.userType) {
      User.findOneAndUpdateAsync({ _id: foundUser._id }, { $set: { loginStatus: true } }, { new: true }) //eslint-disable-line
        // eslint-disable-next-line
        .then(updateUserObj => {
          if (updateUserObj) {
            const jwtAccessToken = jwt.sign(updateUserObj, config.jwtSecret);
            const returnObj = {
              success: true,
              message: '',
              data: {},
            };
            returnObj.message = 'User Already Exists !';
            returnObj.success = false;
            return res.send(returnObj);
          }
        })
        .error((e) => {
          const err = new APIError(`error in updating user details while login ${e}`, httpStatus.INTERNAL_SERVER_ERROR);
          next(err);
        });
    } else {
      const otpValue = Math.floor(100000 + Math.random() * 900000); //eslint-disable-line
      const emailToken = Math.floor(700000000000 + Math.random() * 900000); //eslint-disable-line
      const user = new User({
        email: req.body.email,
        password: req.body.password,
        userType: req.body.userType,
        fname: req.body.fname,
        lname: req.body.lname,
        bikeName: req.body.bikeName,
        ownBike: req.body.ownBike,
        phoneNo: req.body.phoneNo,
        otp: otpValue,
        emailToken: emailToken,
        loginStatus: true,
        addressInfo: {
          city: req.body.city,
        },
      });
      user
        .saveAsync()
        .then((savedUser) => {
console.log(savedUser);          
          var username = 'admin';
          var password = 'admin';
          var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
          request.post({
            headers: {
              'content-type': 'application/x-www-form-urlencoded',
              "Authorization": auth
            },
            url: `${config.aemAuthorUrl}/bin/createPage`,
            form: {
              entity: 'user',
              pageId: savedUser._id.toString(),
              title: savedUser.fname.toString(),
              
            }
          }, function (error, response, dataReturned) {
            

            var data = JSON.parse(dataReturned);
              if (error) {
              returnObj.data.story = null;
              returnObj.message = 'Error Creating User';
              returnObj.success = false;
              res.send(returnObj);
            } 
            else{
            console.log(dataReturned);
            const returnObj = {
            success: true,
            message: '',
            data: {},
            };
            User.findOneAndUpdate(
                { _id: savedUser._id },
                { $set: { userUrl: dataReturned.pagePath } }
              ).then((updatedUser) => {
              const jwtAccessToken = jwt.sign(savedUser, config.jwtSecret);
              returnObj.data.jwtAccessToken = `JWT ${jwtAccessToken}`;
              var obj = { email: savedUser.email, profilePicture: { srcPath: savedUser.profilePicture }, userId: savedUser._id, phone: savedUser.phoneNo, firstName: savedUser.fname, lastName: savedUser.lname };
              returnObj.data.user = obj;
              returnObj.message = 'user created successfully';
              res.send(returnObj);
                
              })
            }




          })
          


        })
        .error(e => next(e));
    }
  })
}

function sendSms(smsText, phoneNo) {
  var num = phoneNo.slice(-10);
  axios.get('http://www.sms.wstechno.com/ComposeSMS.aspx?username=Isetsol-170841&password=Wsc@4141@&sender=WSTECH&&to=' + num + '&message=Please Use this OTP for resetting the password for your Royal Enfield Account.OTP is :' + smsText + '&priority=1&dnd=1&unicode=0')
    .then(function (response) {
      console.log('otp has been sent');
    })
    .catch(function (error) {
      console.log(error);
      console.log("Error in sending sms");
    });
}


function forgot(req, res, next) {
  var phoneNo = req.body.phoneNo;
  User.find({ phoneNo: phoneNo }).then((user) => {
    if (user && user.length > 0) {
      const otpValue = Math.floor(100000 + Math.random() * 900000);
      user[0].otp = otpValue;
      user[0].save();
      sendSms(otpValue, phoneNo);
      res.send("otp has been sent");

    }
    else {
      res.send("User doesnt exist");
    }
  });
}


/**
 * Update existing user
 * @property {Object} req.body.user - user object containing all fields.
 * @returns {User}
 */
function update(req, res, next) {
  const user = req.user;
  user.fname = req.body.fname ? req.body.fname : user.fname;
  user.lname = req.body.lname ? req.body.lname : user.lname;
  user.email = req.body.email ? req.body.email : user.email;
  user.dob = req.body.dob ? req.body.dob : user.dob;
  user.gender = req.body.gender ? req.body.gender : user.gender;
  user.phoneNo = req.body.phoneNo ? req.body.phoneNo : user.phoneNo;
  user.homeAddressInfo.address = req.body.homeAddress ? req.body.homeAddress : user.homeAddressInfo.address;
  user.homeAddressInfo.city = req.body.city ? req.body.city : user.homeAddressInfo.city;
  user.homeAddressInfo.state = req.body.state ? req.body.state : user.homeAddressInfo.state;
  user.homeAddressInfo.country = req.body.country ? req.body.country : user.homeAddressInfo.country;
  user.homeAddressInfo.streetLocalityName = req.body.streetLocalityName ? req.body.streetLocalityName : user.streetLocalityName;
  user.homeAddressInfo.buildingNameFlatNo = req.body.buildingNameFlatNo ? req.body.buildingNameFlatNo : user.buildingNameFlatNo;
  user.homeAddressInfo.Landmark = req.body.Landmark ? req.body.Landmark : user.Landmark;
  user.officeAddressInfo.address = req.body.officeAddress ? req.body.officeAddress : user.officeAddressInfo.address;
  user.bikeOwned = req.body.bikeOwned ? req.body.bikeOwned : user.bikeOwned;
  user.favouriteQuote = req.body.favouriteQuote ? req.body.favouriteQuote : user.favouriteQuote;
  user.favouriteRideExperience = req.body.favouriteRideExperience ? req.body.favouriteRideExperience : user.favouriteRideExperience;
  user
    .saveAsync()
    .then((savedUser) => {

      var userDetailEditData =
        {
          ResourceIdentifier: 204,
          EntityType: 9011,
          EntityId: 24,
          FirstName: savedUser.fname,
          LastName: savedUser.lname,
          DateofBirth: savedUser.dob,
          MobileNo: savedUser.phoneNo,
          EmailID: savedUser.email,
          Gender: savedUser.gender,
          HomeAddress: savedUser.homeAddressInfo.address,
          OfficeAddress: savedUser.officeAddressInfo.address,
          CityName: savedUser.homeAddressInfo.city,
          StateName: savedUser.homeAddressInfo.state,
          CountryName: savedUser.homeAddressInfo.country,
          StreetLocalityName: savedUser.homeAddressInfo.streetLocalityName,
          BuildingNameFlatNo: savedUser.homeAddressInfo.buildingNameFlatNo,
          Landmark: savedUser.homeAddressInfo.Landmark,
        };


      var userDetailEdit = JSON.stringify(userDetailData);

      var userDetailEditOptions = {
        url: `${config.dmsApiUrl}`,
        method: 'POST',
        headers: 'text/plain',
        body: userDetailEdit
      };

      var headers = {
        'Content-Type': 'text/plain'
      };

      request(userDetailEditOptions, function (error, response, body) {
        if (!error && response.statusCode == 200) {
          // Print out the response body
          console.log(body);
          res.send("successfully saved");
        } else {
          console.log(error);
        }
      });
    })
    .error(e => next(e));
}


/**
 * Delete user.
 * @returns {User}
 */
function remove(req, res, next) {
  const user = req.user;
  user
    .removeAsync()
    .then((deletedUser) => {
      const returnObj = {
        success: true,
        message: 'user deleted successfully',
        data: deletedUser,
      };
      res.send(returnObj);
    })
    .error(e => next(e));
}

/**
 * Load user and append to req.
 */
function load(req, res, next, id) {
  User.get(id)
    .then((user) => {
      req.user = user; // eslint-disable-line no-param-reassign
      return next();
    })
    .error(e => next(e));
}

function hashed(password) {
  console.log(password);
  return new Promise((resolve, reject) => {
    bcrypt.genSalt(10, (err, salt) => {
      if (err) {
        reject(err);
      }
      bcrypt.hash(password, salt, (hashErr, hash) => {
        if (hashErr) {
          reject(hashErr);
        }
        resolve(hash);
      });
    });
  });
}


function forgotReset(req, res, next) {
  var phoneNo = req.body.phoneNo;
  User.findOne({ phoneNo: req.body.phoneNo }).then((user) => {
    if (user) {
      var password = req.body.password;
      hashed(password).then((newhash) => {
        user.password = newhash;
        user.save().then((doc) => {
          res.send("password Updated");
        });
      });
    }
    else {
      res.send('User doesnt exist');
    }
  });
}

function reset(req, res, next) {
  var userId = req.body.userId;
  // var jwtAccessToken = req.body.jwtAccessToken;
  User.get(userId)
    .then((user) => {
      if (user) {
        var password = req.body.password;
        hashed(password).then((newhash) => {

          console.log(newhash);
          user.password = newhash;
          user.save().then((doc) => {
            res.send("password Updated");
          });
        });
      }
      else {
        res.send('User doesnt exist');
      }
    });
}

function changepassword(req, res, next) {
  var userId = req.body.userId;
  // var jwtAccessToken = req.body.jwtAccessToken;
  User.get(userId)
    .then((user) => {
      if (user) {

        var oldPassword = req.body.OldPassword;
        var newPassword = req.body.newPassword;
        user.comparePassword(oldPassword, function (err, isMatch) {
          if (err) throw err;
          console.log(isMatch);
          if (isMatch === true) {
            user.password = newPassword;
            user.save().then((doc) => {
              res.send("password Updated");
            });
          }
          else {
            res.send("password didn't match");
          }
        });
      }
      else {
        res.send("User doesnt exist");
      }
    })
    .error(e => next(e));
  next();
}

function getUserDetails(req, res, next) {
  var id = req.query.jsonString;
  
  User.findOne({ _id: id }).then((doc) => {

    var obj = {
      firstName: doc.fname,
      address: { addressAsText: doc.addressInfo.city },
      lastName: doc.lname,
      coverImage: { srcPath: doc.coverImage },
      profilePicture: { srcPath: doc.profilePicture },
      ownerBikeDetails: doc.bikename,
      aboutMe: doc.aboutMe,
      listofTags: doc.listofTags
    }
    console.log(obj);
    res.send(obj);
  });
}

function userInterests(req, res, next) {
  var id = req.body.userId;
  User.findOne({ _id: id }).then((docs) => {
    var i;
    for (i = 0; i < req.body.interests.length; i++) {
      docs.listofTags.push({ userTags: req.body.interests[i] });
    }
    docs.save().then((user) => {
      res.send("Updated User Interests");
    });
  });
}

export default {
  load,
  get,
  create,
  update,
  remove,
  forgot,
  changepassword,
  reset,
  getUserDetails,
  userInterests,
  forgotReset,
};




