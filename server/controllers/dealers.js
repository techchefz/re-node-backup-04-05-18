
import Dealer from '../models/dealers';

function get(req, res) {
  // connect to mongodb and find list of dealers and return the response
  return res.send({ success: true, message: 'dealer found', data: req.dealers });
}

function create(req, res, next) {
  Dealer.findOneAsync({
    $and: [{ dealerName: req.body.dealerName }, { placeId: req.body.placeId }]
  }).then((foundDealer) => {
    if (foundDealer !== null) {
      const returnObj = {
        success: true,
        message: '',
        data: {},
      };
      returnObj.message = 'dealer already exist';
      returnObj.success = false;
      return res.send(returnObj);
    } else {
      const dealer = new Dealer({

        dealer_id: req.body.dealer_id,
        DealerName: req.body.DealerName,
        BranchName: req.body.BranchName,
        address: req.body.address,
        contact_number: req.body.contact_number,
        isServiceApplicable: req.body.isServiceApplicable,
        isSalesApplicable: req.body.isSalesApplicable,
        StartTime: req.body.StartTime,
        EndTime: req.body.EndTime,
        Week_Off: req.body.Week_Off,
        image: req.body.image,
        email: req.body.email,
        review: req.body.review
      });
      dealer
        .saveAsync()
        .then((savedDealer) => {
          const returnObj = {
            success: true,
            message: '',
            data: {},
          };
          returnObj.data.dealer = savedDealer;
          returnObj.message = 'dealer created successfully';
          res.send(returnObj);
        })
        .error(e => next(e));
    }
  });
}
function update(req, res, next) {
  //code to update dealer details goes here ..
}


function remove(req, res, next) {
  // code to remove dealer goes here..
}

export default {
  get,
  create,
  update,
  remove,
};
