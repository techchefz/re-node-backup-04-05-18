
import Story from '../models/tripStory';
import multer from 'multer';
import request from "request";
import config from '../../config/env';

function getStory(req, res, next) {

  var storyId;
  var nextStory = {
    imagePath: null,
    title: null
  };

  var previousStory = {
    imagePath: null,
    title: null
  };

  if (req.body.requestContentJSON) { storyId = req.body.requestContentJSON };

  //To find previos Story
  Story.find({ '_id': { '$lt': storyId } }, 'tripStoryImages storyTitle').sort({ '_id': -1 }).limit(1).then((data) => {
    if (data[0].tripStoryImages.length > 0) {
      previousStory = {
        thumbnailImagePath: data[0].tripStoryImages[0].srcPath,
        title: data[0].storyTitle
      }
    }
  }, (err) => {
    console.log(err);
  });

  //To find next Story
  Story.find({ '_id': { '$gt': storyId } }, 'tripStoryImages storyTitle').sort('_id').limit(1).then((data) => {
    if (data[0].tripStoryImages.length > 0) {
      nextStory = {
        thumbnailImagePath: data[0].tripStoryImages[0].srcPath,
        title: data[0].storyTitle
      }
    }
  }, (err) => {
    console.log(err);
  });

  Story.findOne({ _id: storyId }).populate('postedBy', 'fname lname profilePicture').then((doc) => {
    var returnObj =
      {
        tripStoryId: doc._id,
        title: doc.storyTitle,
        tripStoryBody: doc.storyBody,
        summary: doc.storySummary,
        postedOn: doc.postedOn,
        coverImage: { srcPath: doc.coverImage },
        postedByUser: {
          firstName: doc.postedBy.fname,
          lastName: doc.postedBy.lname,
          profilePicture: { srcPath: doc.postedBy.profilePicture }
        },
        tripImages: doc.tripStoryImages,
        categories: { category: doc.categoryName },
	};
    returnObj.previousStory = previousStory;
    returnObj.nextStory = nextStory;
console.log("----------------=--=-=-=-=--=-=-=-=-=--=-=-=-");    
console.log(returnObj);
    res.send(returnObj);
  }, (err) => {
    res.send(err);
  });
}



function create(req, res, next) {
  console.log("Instory Create ..");
console.log(req);
  var imagePath, imageName;
  var arrayFilePath = [];
  const returnObj = {
    success: true,
    message: '',
    data: {},
  };

  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './node/assets/TripStory');
    },
    filename: function (req, file, cb) {
      imageName = Date.now() + file.originalname;
      cb(null, imageName);
      imagePath = './node/assets/TripStory' + imageName;
    }
  });

  const uploadObj = multer({ storage: storage }).array('tripImages', 10);
  uploadObj(req, res, (err) => {
    if (err) {
      returnObj.data.story = null;
      returnObj.message = 'Error Creating Story';
      returnObj.success = false;
      res.send(returnObj);
    } else {
      req.files.forEach(file => {
        arrayFilePath.push({ srcPath: file.path });
      });

      const story = new Story({
        storyTitle: req.body.storyTitle,
        storyBody: req.body.editordata,
        postedOn: Date.now(),
        storySummary: req.body.storySummary,
        postedBy: req.body.postedBy,
        storyTag: req.body.storyTag,
        categoryName: req.body.storyCategory,
        tripStoryImages: arrayFilePath,
      });
      story
        .saveAsync()
        .then((savedStory) => {
          var username = 'admin';
          var password = 'admin';
          var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
          request.post({
            headers: {
              'content-type': 'application/x-www-form-urlencoded',
              "Authorization": auth
            },
            url: `${config.aemAuthorUrl}/bin/createPage`,
            form: {
              entity: 'trip-story',
              pageId: savedStory._id.toString(),
              title: savedStory.storyTitle,
              category: savedStory.categoryName,
              pageProperties: JSON.stringify({
		"title" : req.body.storyTitle,
		"author" : req.body.postedByUserName,
		"category" : req.body.storyCategory,
                "summary": req.body.storySummary,
                "tags": req.body.storyTag,
                "thumbnailImagePath": arrayFilePath[0].srcPath,
                "postedBy": req.body.postedBy,
                "thumbnailImagePathAltText":req.body.storyTitle
              })
            }
          }, function (error, response, data) {
            var data = JSON.parse(data);
            if (error) {
              returnObj.data.story = null;
              returnObj.message = 'Error Creating Story';
              returnObj.success = false;
              res.send(returnObj);
            } else {
              Story.findOneAndUpdate(
                { _id: savedStory._id },
                { $set: { storyUrl: data.pagePath } }
              ).then((updatedStory) => {
                returnObj.data.story = savedStory;
                returnObj.data.story.storyUrl = data.pagePath;
                returnObj.message = 'story created successfully';
                res.send(returnObj);
              })
            }
          });

        })
        .error(e => next(e));
    }
  })
}

function update(req, res, next) {
  Story.findOneAndUpdate({ _id: req.body._id }, {
    $set: {
      storyTitle: req.body.storyTitle,
      storyBody: req.body.storyBody,
      tripImages: req.body.tripImages,
      storyUrl: req.body.storyUrl
    }
  })
    .then((savedStory) => {
      const returnObj = {
        success: true,
        message: 'Story updated successfully',
        data: savedStory,
      };
      res.send(returnObj);
    })
    .error(e => next(e));
}


function remove(req, res, next) {
  // code to delete story goes here...
  Story.find({ _id: req.body._id }).then((doc) => {
    if (doc[0].postedBy == req.body.userid) {
      doc.remove();
    }

    else {
      res.send("invalid user");
    }
  })
}

function getStories(req, res, next) {
  Story.getStories(parseInt(req.body.skip), parseInt(req.body.limit)).then((stories) => {
    const returnObj = {
      success: true,
      message: '',
      data: {},
    };
    returnObj.data.stories = stories;
    returnObj.message = 'stories retrieved successfully';
    res.send(returnObj);
  })
    .error((e) => next(e));
}


function getmystory(req, res, next) {
  var id = req.body.tripstoryid;
  Story.find({ _id: id }).populate(
    {
      path: 'comment',
      model: 'comments',
      populate: {
        path: 'Replyid userdetailscomments',
        populate: { path: 'userdetailsreplies' }
      },
    }).then((tripstory) => {
      res.send(tripstory[0]);
    });
}




export default {
  getStories,
  getStory,
  create,
  update,
  remove,
  getmystory
};
