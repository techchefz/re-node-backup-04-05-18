import Subscribe from '../models/subscribe';

function subscribed(req, res) {
	const email = req.body.email;
	var subscribe = new Subscribe({
		email: email
	});

	subscribe.save().then((doc) => {
		res.send(doc);
	}, (e) => {
		console.log(e);
	});
}

export default {
	subscribed
};
