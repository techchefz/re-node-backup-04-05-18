import BookTestRide from '../models/book-test-ride';

function create(req, res, next) {
    const bookTestRide = new BookTestRide({
        fName: req.body.fName,
        lName: req.body.lName,
        email: req.body.email,
        bikeName: req.body.bikeName,
        countryName: req.body.countryName,
        stateName: req.body.stateName,
        cityName: req.body.cityName,
        dealerName: req.body.dealerName,
        Date: req.body.Date,
        mobile: req.body.mobile
    });
    bookTestRide
        .saveAsync()
        .then((savedTestRide) => {
            res.send("Test Ride booked successfully");
        })
};

export default {
    create
}