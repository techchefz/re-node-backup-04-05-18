import Ride from '../models/ride';
import { aemcontrol } from './aemcontroller';
import multer from 'multer';
import request from "request";
import config from '../../config/env';
import sortJsonArray from 'sort-json-array';
import geoLib from 'geo-lib';

function getRide(req, res, next) {

}

function create(req, res, next) {
  var imagePath, imageName;
  var arrayFilePath = [
    { srcPath: 'http://d27k8xmh3cuzik.cloudfront.net/wp-content/uploads/2017/05/Cover9.jpg' },
    { srcPath: 'https://i.ytimg.com/vi/OCxEiIJChCg/maxresdefault.jpg' }
  ];
  var returnObj = {
    success: true,
    message: '',
    data: {},
  };
  const ride = new Ride({
    rideName: req.body.rideName,
    startPoint: {
      name: req.body.startPoint,
      latitude: req.body.startLatitude,
      longitude: req.body.startLongitude
    },
    endPoint: {
      name: req.body.endPoint,
      latitude: req.body.endLatitude,
      longitude: req.body.endLongitude
    },
    durationInDays: req.body.durationInDays,
    rideDetails: req.body.rideDetails,
    startDate: req.body.startDate,
    endDate: req.body.endDate,
    terrain: req.body.terrain,
    createdByUser: req.body.userId,
    startTime: `${req.body.startTimeHours + ':' + req.body.startTimeMins + ':' + req.body.startTimeZone}`,
    totalDistance: req.body.totalDistance,
    rideImages: arrayFilePath,
    personalInfo: {
      fName: req.body.fName,
      lName: req.body.lName,
      gender: req.body.gender,
      email: req.body.email,
      password: req.body.password,
      isRoyalEnfieldOwner: req.body.moto,
      dob: req.body.dob,
      city: req.body.city,
      bikeName: req.body.bikeOwned
    },
    waypoints: req.body.wayPoints,
    rideCategory: req.body.rideCategory,
  });
  ride
    .saveAsync()
    .then((savedRide) => {
      var username = 'admin';
      var password = 'admin';
      var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
      request.post({
        headers: {
          'content-type': 'application/x-www-form-urlencoded',
          "Authorization": auth
        },
        url: `${config.aemAuthorUrl}/bin/createPage`,
        form: {
          entity: savedRide.rideCategory,
          pageId: savedRide._id.toString(),
          title: savedRide.rideName,
          pageProperties: JSON.stringify({
            "startLocation": req.body.startPoint,
            "endLocation": req.body.endPoint,
            "startDate": req.body.startDate,
            "endDate": req.body.endDate,
            "terrain": req.body.terrain,
            "createdByUser": req.body.userId,
            
          })
        }
      }, function (error, response, data) {
        var resData = JSON.parse(data);
        if (error) {
          returnObj.data.ride = null;
          returnObj.message = 'Error Creating ride';
          returnObj.success = false;
          res.send(returnObj);
        } else {
          Ride.findOneAndUpdate(
            { _id: savedRide._id },
            { $set: { ridePageUrl: resData.pagePath } }
          ).then((updatedride) => {
            returnObj.data.ride = savedRide;
            returnObj.data.ride.ridePageUrl = resData.pagePath.toString();
            returnObj.message = 'ride created successfully';
            res.send(returnObj);
          })
        }
      });
    })
    .error(e => next(e));
}

function update(req, res, next) {

}


function remove(req, res, next) {
  // code to remove ride goes here..
  Ride.find({ _id: req.body.rideId }).remove().then((removedDoc) => {
    res.send("ride removed successfully");
  });
}


/**
 * Get Rides list.
 * @property {number} req.body.skip - Number of rides to be skipped.
 * @property {number} req.body.limit - Limit number of rides to be returned.
 * @returns {User[]}
 */

function getRide(req, res, next) {
  Ride.find({ _id: req.body.rideId }).populate('createdByUser').then((ride) => {
    console.log(ride)
    res.send(ride[0]);
  });
}

function getRides(req, res, next) {
  Ride.find({ _id: req.body.rideId }).then((rides) => {
    res.send(rides);
  });
}

function getRidesAroundMeBackup(req, res, next) {
  // var Userlat = parseFloat(req.body.latitude);
  // var Userlong = parseFloat(req.body.longitude);
  // Ride.find().then((doc) => {
  //   for (i = 0; i < doc.length; i++) {
  //     var result = geoLib.distance({
  //       p1: { lat: Userlat, lon: Userlong },
  //       p2: { lat: doc[i].startPoint.latitude, lon: doc[i].startPoint.longitude }
  //     });
      
  //     doc[i].calculatedDistance = result.distance;
  //   }
  //   var newarray = sortBy(doc, 'calculatedDistance');
  //   const result = {
  //     object: newarray,
  //   };
  //   console.log('--------------------');
  //   console.log(newarray);
  //   res.send(result);
  // });
}

function getRidesAroundMe(req, res, next) {
  var Userlat = parseFloat(req.body.latitude);
  var Userlong = parseFloat(req.body.longitude);
  Ride.find().then((doc) => {
    const result = {
      object: doc,
    };
    res.send(result);
  });
}

export default {
  getRides,
  getRide,
  create,
  update,
  remove,
  getRidesAroundMe
};
