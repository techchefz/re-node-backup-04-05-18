import Bike from '../models/bike';

function get(req, res) {
  return res.send({ success: true, message: 'bike found', data: req.bike });
}

function create(req, res, next) {
  Bike.findOneAsync({
    $and: [{ bikeName: req.body.bikeName }, { bikeCategory: req.body.bikeCategory }]
  }).then((foundBike) => {
    if (foundBike !== null) {
      const returnObj = {
        success: true,
        message: '',
        data: {},
      };
      returnObj.message = 'bike already exist';
      returnObj.success = false;
      return res.send(returnObj);
    } else {
      const bike = new Bike({
        bikeModel: req.body.bikeModel,
        bikeName: req.body.bikeName,
        bikeCategory: req.body.bikeCategory,
      });
      bike
        .saveAsync()
        .then((savedBike) => {
          const returnObj = {
            success: true,
            message: '',
            data: {},
          };
          returnObj.data.bike = savedBike;
          returnObj.message = 'bike created successfully';
          res.send(returnObj);
        })
        .error(e => next(e));
    }
  });
}

function update(req, res, next) {
  //code to update bike details goes here ..
  Bike.find({_id: req.body._id}).then((doc)=>{
    doc[0].bikeModel = req.body.bikeModel ? req.body.bikeModel : doc[0].bikeModel,
    doc[0].bikeName = req.body.bikeName ? req.body.bikeName : doc[0].bikeName,
    doc[0].bikeCategory = req.body.bikeCategory  ? req.body.bikeCategory : doc[0].bikeCategory
  });
  bike
  .saveAsync()
  .then((updatedBike) => {
    const returnObj = {
      success: true,
      message: '',
      data: {},
    };
    returnObj.data.bike = updatedBike;
    returnObj.message = 'bike updated successfully';
    res.send(returnObj);
  })
  .error(e => next(e));
}


function remove(req, res, next) {
  // code to remove bike goes here..
  Bike.remove({bikeName: req.body.bikeName}).then((removedBike) => {
    const returnObj = {
      success: true,
      message: '',
      data: {},
    };
    returnObj.data.bike = removedBike;
    returnObj.message = 'bike removed successfully';
    res.send(returnObj);
  })
  .error(e => next(e));
}

export default {
  get,
  create,
  update,
  remove,
};
