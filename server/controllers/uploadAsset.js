import Uploadeddata from '../models/uploadeddata';
import express from 'express';
const multer = require('multer');

function upload(req, res, next) {
    console.log(req.body);
    var imagePath, imageName;

    const storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, './assets/TripStory');
        },
        filename: function (req, file, cb) {
            imageName = Date.now() + file.originalname;
            cb(null, imageName);
            imagePath = './assets/TripStory' + imageName;
        }
    });

    const uploadObj = multer({ storage: storage }).array('myImage', 10);
    var arrayFilePath = [];
    uploadObj(req,res,(err)=> {
        if(err){
            res.send(err);
        } else {
            console.log(req.files);
            req.files.forEach(file => {
              arrayFilePath.push(file.path);  
            });
            console.log(req.body)
            res.send(arrayFilePath);
        }
    })

}

function fetch(req, res, next) {
    Uploadeddata.find().then((doc) => {
        var docs = { doc: doc }
        res.send(doc);
    });
}

export default {
    fetch,
    upload
};




