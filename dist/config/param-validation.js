'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  // POST /api/users/register
  createUser: {
    body: {
      email: _joi2.default.string().required(),
      password: _joi2.default.string().required(),
      phoneNo: _joi2.default.string().required()
    }
  },

  // POST /api/auth/login
  login: {
    body: {
      email: _joi2.default.string().required(),
      password: _joi2.default.string().required(),
      userType: _joi2.default.string().required()
    }
  },

  // POST /api/bikes/register
  createBike: {
    body: {
      bikeModel: _joi2.default.string().required(),
      bikeName: _joi2.default.string().required(),
      bikeCategory: _joi2.default.string().required()
    }
  },

  // POST /api/stories/create
  createTripStory: {
    body: {
      storyTitle: _joi2.default.string().required()
    }
  },

  // POST /api/stories/all
  getStories: {
    body: {
      limit: _joi2.default.string().required()
    }
  },

  // POST /api/stories/
  getStory: {
    body: {}
  },

  // POST /api/rides/create
  createRide: {
    body: {
      rideName: _joi2.default.string().required()
    }
  },

  // POST /api/rides/all
  getRides: {
    body: {
      limit: _joi2.default.string().required()
    }
  },

  // POST /api/rides/
  getRide: {
    body: {
      rideId: _joi2.default.string().required()
    }
  },

  sendOtp: {
    body: {
      phoneNo: _joi2.default.string().required()
    }
  },

  getBikeDetail: {
    body: {
      phoneNo: _joi2.default.string().required()
    }
  },

  findState: {
    body: {
      state: _joi2.default.string().required()
    }
  },

  findCity: {
    body: {
      city: _joi2.default.string().required()
    }
  }
};
module.exports = exports['default'];
//# sourceMappingURL=param-validation.js.map
