'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var recaptchaSchema = new _mongoose2.default.Schema({
	Randomnumber: {
		type: Number,
		required: true,
		minlength: 1
	},

	publickey: {
		type: String,
		require: true,
		minlength: 6
	},
	privatekey: {
		type: String,
		required: true,
		minlength: 1
	}
});

exports.default = _mongoose2.default.model('Recaptcha', recaptchaSchema);
module.exports = exports['default'];
//# sourceMappingURL=recaptcha.js.map
