'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _APIError = require('../helpers/APIError');

var _APIError2 = _interopRequireDefault(_APIError);

var _replies = require('./replies');

var _replies2 = _interopRequireDefault(_replies);

var _user = require('./user');

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*** 
comments Schema 
***/

var moment = require('moment');
var Schema = _mongoose2.default.Schema;
var commentSchema = new _mongoose2.default.Schema({
    Replyid: [{ type: _mongoose2.default.Schema.Types.ObjectId, ref: 'replies', default: null }],
    commentBody: { type: String, default: null },
    date: { type: String, default: moment().format("D MMM YYYY") },
    userdetailscomments: { type: _mongoose2.default.Schema.Types.ObjectId, ref: 'User', default: null }
});

exports.default = _mongoose2.default.model('comments', commentSchema);
module.exports = exports['default'];
//# sourceMappingURL=comments.js.map
