'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _APIError = require('../helpers/APIError');

var _APIError2 = _interopRequireDefault(_APIError);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*** 
comments Schema 
***/

var Schema = _mongoose2.default.Schema;
var bookTestRideSchema = new _mongoose2.default.Schema({
    fName: { type: String, default: null },
    lName: { type: String, default: null },
    email: { type: String, default: null },
    bikeName: { type: String, default: null },
    countryName: { type: String, default: null },
    stateName: { type: String, default: null },
    cityName: { type: String, default: null },
    dealerName: { type: String, default: null },
    Date: { type: String, default: null },
    mobile: { type: Number, default: null }
});

exports.default = _mongoose2.default.model('bookTestRide', bookTestRideSchema);
module.exports = exports['default'];
//# sourceMappingURL=book-test-ride.js.map
