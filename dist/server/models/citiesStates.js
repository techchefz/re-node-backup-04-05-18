"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = _mongoose2.default.Schema;
var getCitiesSchema = new Schema({
    "CityId": { type: Number, default: null },
    "CityCode": { type: String, default: null },
    "CityName": { type: String, default: null },
    "StateId": { type: Number, default: null },
    "StateCode": { type: String, default: null },
    "StateName": { type: String, default: null },
    "Country": { type: String, default: null }
});

exports.default = _mongoose2.default.model('cities', getCitiesSchema);
module.exports = exports["default"];
//# sourceMappingURL=citiesStates.js.map
