'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _validator = require('validator');

var _validator2 = _interopRequireDefault(_validator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var UserSchema = new _mongoose2.default.Schema({
  email: {
    type: String,
    required: true,
    trim: true,
    minlength: 1,
    unique: true,
    validate: {
      validator: _validator2.default.isEmail,
      message: '{VALUE} is not a valid email'
    }
  }
});

exports.default = _mongoose2.default.model('Subscribe', UserSchema);
module.exports = exports['default'];
//# sourceMappingURL=subscribe.js.map
