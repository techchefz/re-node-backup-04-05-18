'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Ride Schema
 */

var Schema = _mongoose2.default.Schema;
var RideSchema = new _mongoose2.default.Schema({
  ridePageUrl: { type: String, default: null },
  rideName: { type: String, default: null },
  startPoint: {
    latitude: { type: Number, default: 0 },
    longitude: { type: Number, default: 0 },
    name: { type: String, default: null }
  },
  endPoint: {
    latitude: { type: Number, default: 0 },
    longitude: { type: Number, default: 0 },
    name: { type: String, default: null }
  },
  waypoints: [{
    latitude: { type: Number, default: 0 },
    longitude: { type: Number, default: 0 },
    name: { type: String, default: null }
  }],
  durationInHours: { type: Number, default: null },
  calculatedDistance: { type: Number, default: 0 },
  durationInDays: { type: Number, default: null },
  rideDetails: { type: String, default: null },
  startDate: { type: String, default: null },
  endDate: { type: String, default: null },
  terrain: { type: String, default: null },
  createdByUser: { type: Schema.Types.ObjectId, ref: 'user' },
  ridersJoined: [{ type: Schema.Types.ObjectId, default: null }],
  startTime: { type: String, default: null },
  totalDistance: { type: Number, default: null },
  rideImages: [{
    srcPath: { type: String, default: null }
  }],
  rideCategory: { type: String, default: null },
  personalInfo: {
    fName: { type: String, default: null },
    lName: { type: String, default: null },
    gender: { type: String, default: null },
    email: { type: String, default: null },
    password: { type: String, default: null },
    isRoyalEnfieldOwner: { type: Boolean, default: null },
    dob: { type: String, default: null },
    city: { type: String, default: null },
    bikeName: { type: String, default: null }
  }
});

exports.default = _mongoose2.default.model('Ride', RideSchema);
module.exports = exports['default'];
//# sourceMappingURL=ride.js.map
