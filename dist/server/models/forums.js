'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _APIError = require('../helpers/APIError');

var _APIError2 = _interopRequireDefault(_APIError);

var _comments = require('./comments');

var _comments2 = _interopRequireDefault(_comments);

var _replies = require('./replies');

var _replies2 = _interopRequireDefault(_replies);

var _user = require('./user');

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Forums Schema
 */

var Schema = _mongoose2.default.Schema;

var ForumSchema = new _mongoose2.default.Schema({
    forumTitle: { type: String, default: null, required: true },
    forumBody: { type: String, default: null, required: true },
    categoryName: { type: String, default: null },
    postedOn: { type: Date, default: null },
    postedBy: { type: Schema.Types.ObjectId, ref: 'User', default: null },
    forumUrl: { type: String, default: null },
    views: { type: Number, default: 0 },
    comment: [{ type: _mongoose2.default.Schema.Types.ObjectId, ref: 'comments', default: null }]
});

/**
 * Statics
 */

ForumSchema.statics = {
    getForumPosts: function getForumPosts(skip, limit) {
        return this.find().skip(skip).limit(limit).execAsync().then(function (stories) {
            if (stories) {
                return stories;
            }
            var err = new _APIError2.default('Error Retrieving Forums!', _httpStatus2.default.NOT_FOUND);
            return Promise.reject(err);
        });
    },
    getForumDetail: function getForumDetail(id) {
        return this.find({ _id: id }).execAsync().then(function (story) {
            if (story) {
                return story;
            }
            var err = new _APIError2.default('No Such Forum Exists !', _httpStatus2.default.NOT_FOUND);
            return Promise.reject(err);
        });
    }
};

exports.default = _mongoose2.default.model('Forum', ForumSchema);
module.exports = exports['default'];
//# sourceMappingURL=forums.js.map
