'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var dealerDetailsSchema = new _mongoose2.default.Schema({
    dealer_id: { type: Object, default: null },
    DealerName: { type: String, default: null },
    BranchName: { type: String, default: null },
    address: {
        city: { type: String, default: null },
        State: { type: String, default: null },
        country: { type: String, default: null },
        pincode: { type: String, default: null },
        lat: { type: String, default: null },
        long: { type: String, default: null }
    },
    contact_number: { type: String, default: null },
    isServiceApplicable: { type: Boolean, default: false },
    isSalesApplicable: { type: Boolean, default: false },
    StartTime: { type: String, default: null },
    EndTime: { type: String, default: null },
    Week_Off: { type: Boolean, default: null },
    image: { type: String, default: null },
    email: { type: String, default: null },
    review: {
        reviewByUser: {
            firstName: { type: String, default: null },
            lastName: { type: String, default: null },
            profilePicture: { type: String, default: null }
        },
        reviewText: { type: String, default: null }
    }

});

exports.default = _mongoose2.default.model('dealer', dealerDetailsSchema);
module.exports = exports['default'];
//# sourceMappingURL=dealers.js.map
