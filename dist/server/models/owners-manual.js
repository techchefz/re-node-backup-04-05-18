'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ownersManualSchema = new _mongoose2.default.Schema({

  name: { type: String, default: null },
  email: { type: String, default: null },
  phone: { type: String, default: null },
  city: { type: String, default: null }

});

exports.default = _mongoose2.default.model('OwnersManual', ownersManualSchema);
module.exports = exports['default'];
//# sourceMappingURL=owners-manual.js.map
