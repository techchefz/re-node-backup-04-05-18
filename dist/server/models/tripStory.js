'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _ref;

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _APIError = require('../helpers/APIError');

var _APIError2 = _interopRequireDefault(_APIError);

var _comments = require('./comments');

var _comments2 = _interopRequireDefault(_comments);

var _replies = require('./replies');

var _replies2 = _interopRequireDefault(_replies);

var _user = require('./user');

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Trip-Story Schema
 */

var Schema = _mongoose2.default.Schema;
var TripStorySchema = new _mongoose2.default.Schema((_ref = {
    storyTitle: { type: String, default: null, required: true },
    storySummary: { type: String, default: null },
    storyBody: { type: String, default: null, required: true }
}, _defineProperty(_ref, 'storySummary', { type: String, default: null }), _defineProperty(_ref, 'categoryName', { type: String, default: null }), _defineProperty(_ref, 'postedOn', { type: Date, default: null }), _defineProperty(_ref, 'postedBy', { type: Schema.Types.ObjectId, ref: 'User', default: null }), _defineProperty(_ref, 'coverImage', { type: String, default: null }), _defineProperty(_ref, 'storyUrl', { type: String, default: null }), _defineProperty(_ref, 'comment', [{ type: _mongoose2.default.Schema.Types.ObjectId, ref: 'comments', default: null }]), _defineProperty(_ref, 'tripStoryImages', [{
    srcPath: { type: String, default: null }
}]), _defineProperty(_ref, 'categoryName', { type: String, default: null }), _ref));

/**
 * Statics
 */

TripStorySchema.statics = {
    getStories: function getStories(skip, limit) {
        return this.find().skip(skip).limit(limit).execAsync().then(function (stories) {
            if (stories) {
                return stories;
            }
            var err = new _APIError2.default('Error Retrieving stories!', _httpStatus2.default.NOT_FOUND);
            return Promise.reject(err);
        });
    },
    getStory: function getStory(id) {
        return this.find({ _id: id }).execAsync().then(function (story) {
            if (story) {
                return story;
            }
            var err = new _APIError2.default('No Such story Exists !', _httpStatus2.default.NOT_FOUND);
            return Promise.reject(err);
        });
    }
};

exports.default = _mongoose2.default.model('TripStory', TripStorySchema);
module.exports = exports['default'];
//# sourceMappingURL=tripStory.js.map
