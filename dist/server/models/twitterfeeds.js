'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var twitterFeedSchema = new _mongoose2.default.Schema({
	User: {
		type: String,
		required: true,
		minlength: 1
	},
	ProfileImage: {
		type: String,
		require: true,
		minlength: 2
	},
	Text: {
		type: String,
		required: true,
		minlength: 1
	},
	Likes: {
		type: Number,
		required: true

	},

	Retweets: {
		type: Number,
		required: true

	}
});

exports.default = _mongoose2.default.model('Twitterfeed', twitterFeedSchema);
module.exports = exports['default'];
//# sourceMappingURL=twitterfeeds.js.map
