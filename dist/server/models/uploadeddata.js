'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var uploadeddata = new _mongoose2.default.Schema({
    url: { type: String },
    title: { type: String },
    description: { type: String },
    date: { type: String },
    time: { type: Number },
    kilometers: { type: Number }
});

exports.default = _mongoose2.default.model('uploadeddata', uploadeddata);
module.exports = exports['default'];
//# sourceMappingURL=uploadeddata.js.map
