'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * News Schema
 */

var Schema = _mongoose2.default.Schema;
var NewsSchema = new _mongoose2.default.Schema({
  comment: [{ type: _mongoose2.default.Schema.Types.ObjectId, ref: 'comments', default: null }],
  newsUrl: { type: String, default: null }
});

exports.default = _mongoose2.default.model('News', NewsSchema);
module.exports = exports['default'];
//# sourceMappingURL=news.js.map
