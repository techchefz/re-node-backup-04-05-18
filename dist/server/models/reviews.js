'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _APIError = require('../helpers/APIError');

var _APIError2 = _interopRequireDefault(_APIError);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*** 
comments Schema 
***/
var moment = require('moment');
var Schema = _mongoose2.default.Schema;

var ReviewsSchema = new _mongoose2.default.Schema({

	reviewEntityId: { type: String, default: null },

	reviewByUser: { type: _mongoose2.default.Schema.Types.ObjectId, ref: 'User', default: null },
	reviewDescription: { type: String, default: null },
	reviewText: { type: String, default: null },
	reviewDateText: { type: String, default: null },
	averageRating: { type: Number, default: 0 },
	averageRatingPercentage: { type: Number, default: 0 },
	reviewCriterias: {

		performance: {
			review: {
				reviewCriteria: { type: String, default: null },
				reviewCriteriaLabel: { type: String, default: null },
				averageRating: { type: Number, default: 0 },
				averagePercentage: { type: Number, default: 0 }
			}
		},
		handling: {
			review: {
				reviewCriteria: { type: String, default: null },
				reviewCriteriaLabel: { type: String, default: null },
				averageRating: { type: Number, default: 0 },
				averagePercentage: { type: Number, default: 0 }
			}
		},
		style: {
			review: {
				reviewCriteria: { type: String, default: null },
				reviewCriteriaLabel: { type: String, default: null },
				averageRating: { type: Number, default: 0 },
				averagePercentage: { type: Number, default: 0 }
			}
		},
		verstality: { review: {
				reviewCriteria: { type: String, default: null },
				reviewCriteriaLabel: { type: String, default: null },
				averageRating: { type: Number, default: 0 },
				averagePercentage: { type: Number, default: 0 }
			}
		},
		ownershipExperience: { review: {
				reviewCriteria: { type: String, default: null },
				reviewCriteriaLabel: { type: String, default: null },
				averageRating: { type: Number, default: 0 },
				averagePercentage: { type: Number, default: 0 }
			}
		}
	}

});

exports.default = _mongoose2.default.model('Reviews', ReviewsSchema);
module.exports = exports['default'];
//# sourceMappingURL=reviews.js.map
