'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _expressValidation = require('express-validation');

var _expressValidation2 = _interopRequireDefault(_expressValidation);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _APIError = require('../helpers/APIError');

var _APIError2 = _interopRequireDefault(_APIError);

var _env = require('../../config/env');

var _env2 = _interopRequireDefault(_env);

var _paramValidation = require('../../config/param-validation');

var _paramValidation2 = _interopRequireDefault(_paramValidation);

var _serviceBooking = require('../controllers/serviceBooking');

var _serviceBooking2 = _interopRequireDefault(_serviceBooking);

var _verify = require('../controllers/verify');

var _verify2 = _interopRequireDefault(_verify);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

router.route('/getdealers').post(_serviceBooking2.default.getDealers);

router.route('/findstate').post(_serviceBooking2.default.findState);

router.route('/findcity').post(_serviceBooking2.default.findCity);

router.route('/findpincode').post(_serviceBooking2.default.findPincode);

router.route('/sendotp').post(_serviceBooking2.default.sendVerificationSms);

router.route('/verify').post(_verify2.default.verifyOtp);

router.route('/getBikeDetail').post(_serviceBooking2.default.getBikeDetail);

router.route('/getSlots').post(_serviceBooking2.default.getSlots);

router.route('/getDates').post(_serviceBooking2.default.getDates);

router.route('/serviceBooking').post(_serviceBooking2.default.serviceBooking);

router.route('/getDate').post(_serviceBooking2.default.getDates);
exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=servicebooking.js.map
