'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _auth = require('./auth');

var _auth2 = _interopRequireDefault(_auth);

var _user = require('./user');

var _user2 = _interopRequireDefault(_user);

var _bike = require('./bike');

var _bike2 = _interopRequireDefault(_bike);

var _tripStory = require('./tripStory');

var _tripStory2 = _interopRequireDefault(_tripStory);

var _ride = require('./ride');

var _ride2 = _interopRequireDefault(_ride);

var _verify = require('./verify');

var _verify2 = _interopRequireDefault(_verify);

var _uploadAsset = require('./uploadAsset');

var _uploadAsset2 = _interopRequireDefault(_uploadAsset);

var _social = require('./social');

var _social2 = _interopRequireDefault(_social);

var _subscribe = require('./subscribe');

var _subscribe2 = _interopRequireDefault(_subscribe);

var _storelocator = require('./storelocator');

var _storelocator2 = _interopRequireDefault(_storelocator);

var _recaptcha = require('./recaptcha');

var _recaptcha2 = _interopRequireDefault(_recaptcha);

var _dealers = require('./dealers');

var _dealers2 = _interopRequireDefault(_dealers);

var _comments = require('./comments');

var _comments2 = _interopRequireDefault(_comments);

var _servicebooking = require('./servicebooking');

var _servicebooking2 = _interopRequireDefault(_servicebooking);

var _replies = require('./replies');

var _replies2 = _interopRequireDefault(_replies);

var _reviews = require('./reviews');

var _reviews2 = _interopRequireDefault(_reviews);

var _search = require('./search');

var _search2 = _interopRequireDefault(_search);

var _news = require('./news');

var _news2 = _interopRequireDefault(_news);

var _bookTestRide = require('./book-test-ride');

var _bookTestRide2 = _interopRequireDefault(_bookTestRide);

var _forums = require('./forums');

var _forums2 = _interopRequireDefault(_forums);

var _cityStates = require('./cityStates');

var _cityStates2 = _interopRequireDefault(_cityStates);

var _ownersManual = require('./owners-manual');

var _ownersManual2 = _interopRequireDefault(_ownersManual);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

/** GET /health-check - Check service health */
router.get('/health-check', function (req, res) {
  return res.send('OK');
});

router.get('/', function (req, res) {
  return res.send('OK');
});

// mount user routes at /verify
router.use('/verify', _verify2.default);

// mount user routes at /users
router.use('/users', _user2.default);

// mount user routes at /users
router.use('/dealers', _dealers2.default);

// mount auth routes at /auth
router.use('/auth', _auth2.default);

// mount auth routes at /bikes
router.use('/bikes', _bike2.default);

// mount stories routes at /stories
router.use('/stories', _tripStory2.default);

//mount comments routes at /comments
router.use('/comments', _comments2.default);
// mount stories routes at /rides
router.use('/rides', _ride2.default);

//mount UploadAsset routes at /uploadAsset
router.use('/uploadAsset', _uploadAsset2.default);

//mount Social routes at /social
router.use('/social', _social2.default);

//mount recaptcha routes at /recaptcha
router.use('/recaptcha', _recaptcha2.default);

//mount subscription routes at /subscribe
router.use('/subscribe', _subscribe2.default);

//mount store routes at /storelocator
router.use('/storelocator', _storelocator2.default);

//mount servicebooking routes at /servicebooking
router.use('/servicebooking', _servicebooking2.default);

//mount replies routes at /replies
router.use('/replies', _replies2.default);

// mount reviews routes at /reviews
router.use('/reviews', _reviews2.default);

//mount search routes at /search
router.use('/search', _search2.default);

//mount news routes at /news
router.use('/news', _news2.default);

//mount forum routes at /fourms 
router.use('/forums', _forums2.default);

//mount forum routes at /fourms 
router.use('/booktestride', _bookTestRide2.default);

//mount cityStates at /cityStates
router.use('/cityStates', _cityStates2.default);

//mount owners-manual at /ownersmanual
router.use('/ownersmanual', _ownersManual2.default);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=index.js.map
