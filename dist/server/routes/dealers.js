'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _expressValidation = require('express-validation');

var _expressValidation2 = _interopRequireDefault(_expressValidation);

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _dealers = require('../controllers/dealers');

var _dealers2 = _interopRequireDefault(_dealers);

var _APIError = require('../helpers/APIError');

var _APIError2 = _interopRequireDefault(_APIError);

var _env = require('../../config/env');

var _env2 = _interopRequireDefault(_env);

var _paramValidation = require('../../config/param-validation');

var _paramValidation2 = _interopRequireDefault(_paramValidation);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

/** POST /api/dealers/register - create new Dealer and return corresponding dealer object and token */
router.route('/register').post(_dealers2.default.create);

router.route('/')
/** GET /api/dealers - Get dealers */
.get(_dealers2.default.get)

/** PUT /api/dealers - Update dealers */
.put(_dealers2.default.update)

/** DELETE /api/bikes - Delete dealers */
.delete(_dealers2.default.remove);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=dealers.js.map
