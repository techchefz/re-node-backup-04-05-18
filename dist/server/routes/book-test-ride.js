'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _expressValidation = require('express-validation');

var _expressValidation2 = _interopRequireDefault(_expressValidation);

var _paramValidation = require('../../config/param-validation');

var _paramValidation2 = _interopRequireDefault(_paramValidation);

var _bookTestRide = require('../controllers/book-test-ride');

var _bookTestRide2 = _interopRequireDefault(_bookTestRide);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

/** POST /api/comments/create - create new Trip Story and return corresponding story object */
router.route('/create').post(_bookTestRide2.default.create);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=book-test-ride.js.map
