'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _expressValidation = require('express-validation');

var _expressValidation2 = _interopRequireDefault(_expressValidation);

var _paramValidation = require('../../config/param-validation');

var _paramValidation2 = _interopRequireDefault(_paramValidation);

var _reviews = require('../controllers/reviews');

var _reviews2 = _interopRequireDefault(_reviews);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

// POST /api/reviews/create
router.route('/create').post(_reviews2.default.create);

// get /api/reviews/reviewsummary
router.route('/reviewsummary').get(_reviews2.default.getReviews);

//get /api/reviews/reviewdetails

router.route('/reviewdetails').get(_reviews2.default.fetch);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=reviews.js.map
