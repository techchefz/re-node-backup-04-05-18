'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _expressValidation = require('express-validation');

var _expressValidation2 = _interopRequireDefault(_expressValidation);

var _paramValidation = require('../../config/param-validation');

var _paramValidation2 = _interopRequireDefault(_paramValidation);

var _replies = require('../controllers/replies');

var _replies2 = _interopRequireDefault(_replies);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();
/** POST /api/replies/create */

router.route('/create').post(_replies2.default.createReplies);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=replies.js.map
