'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _expressValidation = require('express-validation');

var _expressValidation2 = _interopRequireDefault(_expressValidation);

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _APIError = require('../helpers/APIError');

var _APIError2 = _interopRequireDefault(_APIError);

var _env = require('../../config/env');

var _env2 = _interopRequireDefault(_env);

var _paramValidation = require('../../config/param-validation');

var _paramValidation2 = _interopRequireDefault(_paramValidation);

var _bike = require('../controllers/bike');

var _bike2 = _interopRequireDefault(_bike);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

/** POST /api/bikes/register - create new Bike and return corresponding bike object and token */
router.route('/register').post((0, _expressValidation2.default)(_paramValidation2.default.createBike), _bike2.default.create);

router.route('/')
/** GET /api/bikes - Get bike */
.get(_bike2.default.get)

/** PUT /api/bikes - Update bike */
.put(_bike2.default.update)

/** DELETE /api/bikes - Delete bike */
.delete(_bike2.default.remove);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=bike.js.map
