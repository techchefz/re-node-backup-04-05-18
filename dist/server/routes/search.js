'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _solr = require('../controllers/solr');

var _solr2 = _interopRequireDefault(_solr);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

router.route('/').get(_solr2.default.searchSolr);

router.route('/searchride').post(_solr2.default.searchRide);

router.route('/testSms').post(_solr2.default.sendSms);

router.route('/tripStorySearch').post(_solr2.default.tripStorySearch);

router.route('/searchTripOnClick').post(_solr2.default.tripStorySearchOnClick);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=search.js.map
