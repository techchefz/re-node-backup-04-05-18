'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _expressValidation = require('express-validation');

var _expressValidation2 = _interopRequireDefault(_expressValidation);

var _paramValidation = require('../../config/param-validation');

var _paramValidation2 = _interopRequireDefault(_paramValidation);

var _cityStates = require('../controllers/cityStates');

var _cityStates2 = _interopRequireDefault(_cityStates);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

router.route('/getcity').post(_cityStates2.default.getcity);

router.route('/getstate').post(_cityStates2.default.getState);

router.route('/getdealers').post(_cityStates2.default.getDealers);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=cityStates.js.map
