'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _expressValidation = require('express-validation');

var _expressValidation2 = _interopRequireDefault(_expressValidation);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _APIError = require('../helpers/APIError');

var _APIError2 = _interopRequireDefault(_APIError);

var _env = require('../../config/env');

var _env2 = _interopRequireDefault(_env);

var _paramValidation = require('../../config/param-validation');

var _paramValidation2 = _interopRequireDefault(_paramValidation);

var _social = require('../controllers/social');

var _social2 = _interopRequireDefault(_social);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

// POST /api/social/twitter
router.route('/twitter').post(_social2.default.twitterdata).get(_social2.default.twitterdata);

router.route('/reviews').post(_social2.default.fetch);

router.route('/reviews').get(_social2.default.fetch);

router.route('/googleplus').post(_social2.default.googleplusdata);

router.route('/googleplus').get(_social2.default.googleplusdata);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=social.js.map
