'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _expressValidation = require('express-validation');

var _expressValidation2 = _interopRequireDefault(_expressValidation);

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _APIError = require('../helpers/APIError');

var _APIError2 = _interopRequireDefault(_APIError);

var _env = require('../../config/env');

var _env2 = _interopRequireDefault(_env);

var _paramValidation = require('../../config/param-validation');

var _paramValidation2 = _interopRequireDefault(_paramValidation);

var _user = require('../controllers/user');

var _user2 = _interopRequireDefault(_user);

var _verify = require('../controllers/verify');

var _verify2 = _interopRequireDefault(_verify);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

/** POST /api/users/register - create new user and return corresponding user object and token */
router.route('/register').post((0, _expressValidation2.default)(_paramValidation2.default.createUser), _user2.default.create);

//api/users/verifyOtp
router.route('/verifyOtp').post(_verify2.default.verifyOtp);

//api/users/changePassword
router.route('/changePassword').post(_user2.default.changepassword);

//api/users/forgot
router.route('/forgot').post(_user2.default.forgot);

//api/users/resetPassword
router.route('/resetPassword').post(_user2.default.reset);

//api/users/resetPassword using mobile no
router.route('/reset-password-phone').post(_user2.default.forgotReset);

//api/users/userInterests
router.route('/userInterests').post(_user2.default.userInterests);

/**
  * Middleware for protected routes. All protected routes need token in the header in the form Authorization: JWT token
  */

//router.use((req, res, next) => {
//  passport.authenticate('jwt', config.passportOptions, (error, userDtls, info) => { //eslint-disable-line
//    if (error) {
//      const err = new APIError('token not matched', httpStatus.INTERNAL_SERVER_ERROR);
//      return next(err);
//    } else if (userDtls) {
//      req.user = userDtls;
//      next();
//    } else {
//      const err = new APIError(`token not matched ${info}`, httpStatus.UNAUTHORIZED);
//      return next(err);
//    }
//  })(req, res, next);
//});


router.route('/')
/** GET /api/users - Get user */
.get(_user2.default.getUserDetails)

/** PUT /api/users - Update user */
.put(_user2.default.update)

/** DELETE /api/users - Delete user */
.delete(_user2.default.remove);

/** Load user when API with userId route parameter is hit */
router.param('userId', _user2.default.load);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=user.js.map
