"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _forums = require("../models/forums");

var _forums2 = _interopRequireDefault(_forums);

var _forumCategory = require("../models/forumCategory");

var _forumCategory2 = _interopRequireDefault(_forumCategory);

var _request = require("request");

var _request2 = _interopRequireDefault(_request);

var _env = require("../../config/env");

var _env2 = _interopRequireDefault(_env);

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

var _moment = require("moment");

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function timeStampToTime(date) {
    var result = (0, _moment2.default)(date).fromNow();
    var tempDate = result.split(" ");
    if (tempDate[1] === 'hour' || tempDate[1] === 'year' || tempDate[1] === 'days') {
        result = (0, _moment2.default)(date).format("DD MMM YYYY");
    }
    return result;
}

function incrementTopicViews(req, res, next) {
    _forums2.default.findOneAndUpdate({ _id: req.body.forumId }, { $inc: { "views": 1 } }, { new: true }).then(function (updatedForumPost) {
        _forumCategory2.default.findOneAndUpdate({ categoryName: req.body.categoryName }, { $inc: { "totalViewsCount": 1 } }).then(function (doc) {
            res.send(updatedForumPost.views.toString());
        }, function (err) {
            console.log(err);
        });
    }, function (err) {
        console.log(err);
    });
}

function incrementCategoryTopicsCounter(topicCategoryName) {
    _forumCategory2.default.findOneAndUpdate({ categoryName: topicCategoryName }, { $inc: { "totalTopicsCount": 1 } }).then(function (doc) {
        return true;
    }, function (err) {
        console.log(err);
    });
}

function getFourmDetail(req, res, next) {
    var comments = [];
    _forums2.default.findOne({ _id: req.body.obj.entityId }).populate({
        path: 'postedBy comment',
        populate: { path: 'userdetailscomments' }

    }).then(function (doc) {
        console.log("In");
        console.log(doc);
        console.log('-----------------------------');
        doc.comment.forEach(function (comment) {
            var singleComment = {
                "commentText": comment.commentBody,
                "userInfo": {
                    "profilePicture": {
                        "srcPath": comment.userdetailscomments.profilePicture
                    },
                    "fullName": comment.userdetailscomments.fname + ' ' + comment.userdetailscomments.lname
                },
                "commentTime": comment.date
            };
            comments.push(singleComment);
        });
        if (comments.length > 0) {
            var returnObj = {
                topicViews: doc.views,
                replyCount: doc.comment.length ? doc.comment.length : 0,
                forumId: doc._id,
                heading: doc.forumTitle,
                content: doc.forumBody,
                categoryName: doc.categoryName,
                forumTime: timeStampToTime(doc.postedOn),
                user: {
                    fullName: doc.postedBy.fname + doc.postedBy.lname,
                    profilePicture: { srcPath: doc.postedBy.profilePicture }
                },
                comment: comments
            };
        } else {
            var returnObj = {
                topicViews: doc.views,
                replyCount: doc.comment.length ? doc.comment.length : 0,
                forumId: doc._id,
                heading: doc.forumTitle,
                content: doc.forumBody,
                categoryName: doc.categoryName,
                forumTime: timeStampToTime(doc.postedOn),
                user: {
                    fullName: doc.postedBy.fname + doc.postedBy.lname,
                    profilePicture: { srcPath: doc.postedBy.profilePicture }
                }
            };
        }
        console.log(returnObj);
        res.send(returnObj);
    }, function (err) {
        res.send(err);
    });
}

function update(req, res, next) {
    _forums2.default.findOne({ _id: req.body.id }).then(function (foundDoc) {
        foundDoc.forumTitle = req.body.forumTitle ? req.body.forumTitle : foundDoc.forumTitle, foundDoc.forumBody = req.body.forumBody ? req.body.forumBody : foundDoc.forumBody, foundDoc.categoryName = req.body.categoryName ? req.body.categoryName : foundDoc.categoryName;
    });
    _forums2.default.saveAsync().then(function (updatedForum) {
        res.send(updatedForum);
    });
}

function create(req, res, next) {
    var returnObj = {
        success: true,
        message: '',
        data: {}
    };
    var forum = new _forums2.default({
        forumTitle: req.body.title,
        forumBody: req.body.Content,
        categoryName: req.body.category,
        postedOn: Date.now(),
        postedBy: req.body.postedBy
    });
    forum.saveAsync().then(function (savedPost) {
        incrementCategoryTopicsCounter(req.body.category);
        var username = 'admin';
        var password = 'admin';
        var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");

        _request2.default.post({
            headers: {
                'content-type': 'application/x-www-form-urlencoded',
                "Authorization": auth
            },
            url: _env2.default.aemAuthorUrl + "/bin/createPage",
            form: {
                entity: 'forum-topic',
                pageId: savedPost._id.toString(),
                title: savedPost.forumTitle,
                category: savedPost.categoryName
            }
        }, function (error, response, data) {
            var data = JSON.parse(data);
            if (error) {
                returnObj.data.forumPost = null;
                returnObj.message = 'Error Creating Post';
                returnObj.success = false;
                res.send(returnObj);
            } else {
                _forums2.default.findOneAndUpdate({ _id: savedPost._id }, { $set: { forumUrl: data.pagePath } }).then(function (updatedForumPost) {
                    returnObj.data.forumPost = updatedForumPost;
                    returnObj.data.forumPost.forumUrl = data.pagePath;
                    returnObj.message = 'Post created successfully';
                    res.send(returnObj);
                });
            }
        });
    }).error(function (e) {
        return next(e);
    });
}

function getForumPosts(req, res, next) {
    _forums2.default.getForumPosts(parseInt(req.body.skip), parseInt(req.body.limit)).then(function (posts) {
        var returnObj = {
            success: true,
            message: '',
            data: {}
        };
        returnObj.data.post = posts;
        returnObj.message = 'Forum-Posts retrieved successfully';
        res.send(returnObj);
    }).error(function (e) {
        return next(e);
    });
}

exports.default = {
    getFourmDetail: getFourmDetail,
    getForumPosts: getForumPosts,
    create: create,
    incrementTopicViews: incrementTopicViews
};
module.exports = exports["default"];
//# sourceMappingURL=forums.js.map
