'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _comments = require('../models/comments');

var _comments2 = _interopRequireDefault(_comments);

var _news = require('../models/news');

var _news2 = _interopRequireDefault(_news);

var _forums = require('../models/forums');

var _forums2 = _interopRequireDefault(_forums);

var _forumCategory = require('../models/forumCategory');

var _forumCategory2 = _interopRequireDefault(_forumCategory);

var _tripStory = require('../models/tripStory');

var _tripStory2 = _interopRequireDefault(_tripStory);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function incrementCategoryRepliesCounter(topicCategoryName) {
    _forumCategory2.default.findOneAndUpdate({ categoryName: topicCategoryName }, { $inc: { "totalRepliesCount": 1 } }).then(function (doc) {
        return true;
    }, function (err) {
        console.log(err);
    });
}

function createComment(req, res, next) {
    var id = req.body.pageid;
    var userid = req.body.userid;
    var commentBody = req.body.commentBody;
    var category = req.body.category;
    var forumCategory = req.body.forumCategory ? req.body.forumCategory : null;
    console.log("------------------------------------------------xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx----------------------------------------------");
    console.log(req.body);

    if (category === 'TripStory') {
        _tripStory2.default.findOne({ _id: id }).then(function (tripstory) {
            var comment = new _comments2.default({
                commentBody: commentBody,
                userdetailscomments: userid
            });
            comment.save().then(function (doc) {
                tripstory.comment.push(doc._id);
                tripstory.save().then(function (details) {
                    res.send("comments saved");
                }, function (err) {
                    console.log(err);
                });
            });
        });
    } else if (category === 'News') {
        _news2.default.findOne({ _id: id }).then(function (news) {
            var comment = new _comments2.default({
                commentBody: commentBody,
                userdetailscomments: userid
            });
            comment.save().then(function (doc) {
                news.comment.push(doc._id);
                news.save().then(function (details) {
                    res.send("comments saved");
                }, function (err) {
                    console.log(err);
                });
            });
        });
    } else if (category === 'Forums') {
        _forums2.default.findOne({ _id: id }).then(function (forumPost) {
            var comment = new _comments2.default({
                commentBody: commentBody,
                userdetailscomments: userid
            });
            comment.save().then(function (doc) {
                incrementCategoryRepliesCounter(forumCategory);
                forumPost.comment.push(doc._id);
                forumPost.save().then(function (details) {
                    res.send("comments saved");
                }, function (err) {
                    console.log(err);
                });
            });
        });
    }
}

function getComments(req, res, next) {
    var id = req.body.pageid;
    var category = req.body.category;
    console.log(req.body);
    if (category === 'trip-story' || category === 'TripStory') {
        console.log("inside trip");
        _tripStory2.default.find({ _id: id }).populate({
            path: 'comment',
            model: 'comments',
            populate: {
                path: 'Replyid userdetailscomments',
                populate: { path: 'userdetailsreplies' }
            }
        }).then(function (tripstory) {
            console.log(tripstory[0]);
            res.send(tripstory[0]);
        });
    } else if (category === 'News') {
        _news2.default.find({ _id: id }).populate({
            path: 'comment',
            model: 'comments',
            populate: {
                path: 'Replyid userdetailscomments',
                populate: { path: 'userdetailsreplies' }
            }
        }).then(function (news) {
            res.send(news[0]);
        });
    } else if (category === 'Forums') {
        _forums2.default.find({ _id: id }).populate({
            path: 'comment',
            model: 'comments',
            populate: {
                path: 'Replyid userdetailscomments',
                populate: { path: 'userdetailsreplies' }
            }
        }).then(function (forums) {
            res.send(forums[0]);
        });
    }
}

exports.default = {
    createComment: createComment,
    getComments: getComments
};
module.exports = exports['default'];
//# sourceMappingURL=comments.js.map
