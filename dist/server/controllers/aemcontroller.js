'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// they are hardcoded, make them as environment variable
//------------------------------------------------------------------------
var username = 'admin';
var password = 'admin';
var url = 'http://localhost:4502/bin/payment';
// ------------------------------------------------------------------------


function aemcontrol(data) {
    var object = { data: data };

    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");

    _request2.default.post({
        headers: { 'content-type': 'application/x-www-form-urlencoded',
            "Authorization": auth },
        url: url,
        from: object

    }, function (error, response, body) {

        console.log(error);
        console.log(body);
    });
}
exports.default = { aemcontrol: aemcontrol };
module.exports = exports['default'];
//# sourceMappingURL=aemcontroller.js.map
