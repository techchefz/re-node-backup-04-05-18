'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _uploadeddata = require('../models/uploadeddata');

var _uploadeddata2 = _interopRequireDefault(_uploadeddata);

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var multer = require('multer');

function upload(req, res, next) {
    console.log(req.body);
    var imagePath, imageName;

    var storage = multer.diskStorage({
        destination: function destination(req, file, cb) {
            cb(null, './assets/TripStory');
        },
        filename: function filename(req, file, cb) {
            imageName = Date.now() + file.originalname;
            cb(null, imageName);
            imagePath = './assets/TripStory' + imageName;
        }
    });

    var uploadObj = multer({ storage: storage }).array('myImage', 10);
    var arrayFilePath = [];
    uploadObj(req, res, function (err) {
        if (err) {
            res.send(err);
        } else {
            console.log(req.files);
            req.files.forEach(function (file) {
                arrayFilePath.push(file.path);
            });
            console.log(req.body);
            res.send(arrayFilePath);
        }
    });
}

function fetch(req, res, next) {
    _uploadeddata2.default.find().then(function (doc) {
        var docs = { doc: doc };
        res.send(doc);
    });
}

exports.default = {
    fetch: fetch,
    upload: upload
};
module.exports = exports['default'];
//# sourceMappingURL=uploadAsset.js.map
