"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _twitter = require("twitter");

var _twitter2 = _interopRequireDefault(_twitter);

var _util = require("util");

var _util2 = _interopRequireDefault(_util);

var _request = require("request");

var _request2 = _interopRequireDefault(_request);

var _sortJsonArray = require("sort-json-array");

var _sortJsonArray2 = _interopRequireDefault(_sortJsonArray);

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _axios = require("axios");

var _axios2 = _interopRequireDefault(_axios);

var _circularJsonEs = require("circular-json-es6");

var _circularJsonEs2 = _interopRequireDefault(_circularJsonEs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function twitterdata(req, res, next) {
  var hashtag = req.body.hashtag;
  var count = req.body.count;

  var client = new _twitter2.default({
    consumer_key: 'mlG280kRbMSmmtWVoFmQCA4Yz',
    consumer_secret: 'NH7rj8Z6IkA7fCFqSqqshI0Q5GZXwdzafmMd1aoag2n5B1svQ9',
    access_token_key: '17202591-DGsWvfOlUvkjYJUxnMgisDVEOBNtDvRyCnQaGq2Ry',
    access_token_secret: 'LMKq1A1MQGrLiGzOLRAQaiEWMB3hAvkkdtKEJvGbdLcVs'
  });

  var params = {
    q: "#" + hashtag,
    result_type: 'recent',
    tweet_mode: 'extended',
    count: count,
    extended_entities: true
  };

  client.get('search/tweets', params, function (err, data, response) {
    if (err) {
      console.log(err);
    }

    var tweets = data.statuses;
    var i;
    var obj = [];
    var j;
    var text;

    for (i = 0; i < tweets.length; i++) {
      var myobj = {};
      var hashtag = [];
      var linkurl = null;
      var mediaurl = null;

      if (tweets[i].entities.hashtags.length > 0) {
        for (j = 0; j < tweets[i].entities.hashtags.length; j++) {
          hashtag.push(tweets[i].entities.hashtags[j].text);

          myobj.hashtagss = hashtag;
        }
      }
      if (tweets[i].entities.media && tweets[i].entities.media.length > 0) {
        myobj.linkurl = tweets[i].entities.media[0].url;
      }
      if (tweets[i].entities.media && tweets[i].entities.media.length > 0) {
        myobj.mediaurl = tweets[i].entities.media[0].media_url;
      }
      myobj.username = tweets[i].user.name;
      myobj.text = tweets[i].full_text;
      obj.push(myobj);
    }
    var socialfeed = { object: obj };
    res.send(socialfeed);
  });
}

function fetch(req, res, next) {
  var placeid = 'ChIJ7bGcKGThDDkRHLFd_LKUAC4';
  var googlekey = 'AIzaSyBGeqAEoL0x2Gum5Zi5T8eq25XMfgFbnnY';
  var verifyUrl = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + placeid + "&key=" + googlekey;

  (0, _request2.default)(verifyUrl, function (err, response, body) {
    body = JSON.parse(body);
    var Address = body.result.formatted_address;
    var phonenumber = body.result.formatted_phone_number;
    var name = body.result.name;
    var location = body.result.url;
    var ratings = body.result.rating;
    var website = body.result.website;
    var c = body.result.reviews;
    var d = (0, _sortJsonArray2.default)(c, 'rating', 'des');
    var reviewername = d[0].author_name;
    var authorurl = d[0].author_url;
    var profilephoto = d[0].profile_photo_url;
    var authorating = d[0].rating;
    var text = d[0].text;
    var timedescription = d[0].relative_time_description;
    var lat = body.result.geometry.location.lat;
    var lng = body.result.geometry.location.lng;

    var obj = {
      reviewText: text,
      reviewRating: authorating,
      reviewByUser: {
        firstName: reviewername,
        address: { addressAsText: Address },
        profilePicture: { srcPath: profilephoto }
      },
      reviewDateText: timedescription,
      reviewRatingPercentage: authorating * 20
    };

    res.send(obj);
  });
}

function googleplusdata(req, res, next) {
  var apkey = "AIzaSyA3LipyK7I_HaCEgW4jZl59dTlY0-gJvTA";
  var GUrl = "https://www.googleapis.com/plus/v1/activities?query=Royal+Enfield&maxResults=20&orderBy=recent&key=" + apkey;
  _axios2.default.get(GUrl).then(function (response) {
    var clone = _circularJsonEs2.default.parse(_circularJsonEs2.default.stringify(response));
    res.send(clone.data);
  }).catch(function (e) {
    console.log(e);
  });
}

exports.default = {
  twitterdata: twitterdata,
  fetch: fetch,
  googleplusdata: googleplusdata
};
module.exports = exports["default"];
//# sourceMappingURL=social.js.map
