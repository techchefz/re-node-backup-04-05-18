'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ride = require('../models/ride');

var _ride2 = _interopRequireDefault(_ride);

var _aemcontroller = require('./aemcontroller');

var _multer = require('multer');

var _multer2 = _interopRequireDefault(_multer);

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

var _env = require('../../config/env');

var _env2 = _interopRequireDefault(_env);

var _sortJsonArray = require('sort-json-array');

var _sortJsonArray2 = _interopRequireDefault(_sortJsonArray);

var _geoLib = require('geo-lib');

var _geoLib2 = _interopRequireDefault(_geoLib);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getRide(req, res, next) {}

function create(req, res, next) {
  var imagePath, imageName;
  var arrayFilePath = [{ srcPath: 'http://d27k8xmh3cuzik.cloudfront.net/wp-content/uploads/2017/05/Cover9.jpg' }, { srcPath: 'https://i.ytimg.com/vi/OCxEiIJChCg/maxresdefault.jpg' }];
  var returnObj = {
    success: true,
    message: '',
    data: {}
  };
  var ride = new _ride2.default({
    rideName: req.body.rideName,
    startPoint: {
      name: req.body.startPoint,
      latitude: req.body.startLatitude,
      longitude: req.body.startLongitude
    },
    endPoint: {
      name: req.body.endPoint,
      latitude: req.body.endLatitude,
      longitude: req.body.endLongitude
    },
    durationInDays: req.body.durationInDays,
    rideDetails: req.body.rideDetails,
    startDate: req.body.startDate,
    endDate: req.body.endDate,
    terrain: req.body.terrain,
    createdByUser: req.body.userId,
    startTime: '' + (req.body.startTimeHours + ':' + req.body.startTimeMins + ':' + req.body.startTimeZone),
    totalDistance: req.body.totalDistance,
    rideImages: arrayFilePath,
    personalInfo: {
      fName: req.body.fName,
      lName: req.body.lName,
      gender: req.body.gender,
      email: req.body.email,
      password: req.body.password,
      isRoyalEnfieldOwner: req.body.moto,
      dob: req.body.dob,
      city: req.body.city,
      bikeName: req.body.bikeOwned
    },
    waypoints: req.body.wayPoints,
    rideCategory: req.body.rideCategory
  });
  ride.saveAsync().then(function (savedRide) {
    var username = 'admin';
    var password = 'admin';
    var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
    _request2.default.post({
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
        "Authorization": auth
      },
      url: _env2.default.aemAuthorUrl + '/bin/createPage',
      form: {
        entity: savedRide.rideCategory,
        pageId: savedRide._id.toString(),
        title: savedRide.rideName,
        pageProperties: JSON.stringify({
          "startLocation": req.body.startPoint,
          "endLocation": req.body.endPoint,
          "startDate": req.body.startDate,
          "endDate": req.body.endDate,
          "terrain": req.body.terrain,
          "createdByUser": req.body.userId

        })
      }
    }, function (error, response, data) {
      var resData = JSON.parse(data);
      if (error) {
        returnObj.data.ride = null;
        returnObj.message = 'Error Creating ride';
        returnObj.success = false;
        res.send(returnObj);
      } else {
        _ride2.default.findOneAndUpdate({ _id: savedRide._id }, { $set: { ridePageUrl: resData.pagePath } }).then(function (updatedride) {
          returnObj.data.ride = savedRide;
          returnObj.data.ride.ridePageUrl = resData.pagePath.toString();
          returnObj.message = 'ride created successfully';
          res.send(returnObj);
        });
      }
    });
  }).error(function (e) {
    return next(e);
  });
}

function update(req, res, next) {}

function remove(req, res, next) {
  // code to remove ride goes here..
  _ride2.default.find({ _id: req.body.rideId }).remove().then(function (removedDoc) {
    res.send("ride removed successfully");
  });
}

/**
 * Get Rides list.
 * @property {number} req.body.skip - Number of rides to be skipped.
 * @property {number} req.body.limit - Limit number of rides to be returned.
 * @returns {User[]}
 */

function getRide(req, res, next) {
  _ride2.default.find({ _id: req.body.rideId }).populate('createdByUser').then(function (ride) {
    console.log(ride);
    res.send(ride[0]);
  });
}

function getRides(req, res, next) {
  _ride2.default.find({ _id: req.body.rideId }).then(function (rides) {
    res.send(rides);
  });
}

function getRidesAroundMeBackup(req, res, next) {
  // var Userlat = parseFloat(req.body.latitude);
  // var Userlong = parseFloat(req.body.longitude);
  // Ride.find().then((doc) => {
  //   for (i = 0; i < doc.length; i++) {
  //     var result = geoLib.distance({
  //       p1: { lat: Userlat, lon: Userlong },
  //       p2: { lat: doc[i].startPoint.latitude, lon: doc[i].startPoint.longitude }
  //     });

  //     doc[i].calculatedDistance = result.distance;
  //   }
  //   var newarray = sortBy(doc, 'calculatedDistance');
  //   const result = {
  //     object: newarray,
  //   };
  //   console.log('--------------------');
  //   console.log(newarray);
  //   res.send(result);
  // });
}

function getRidesAroundMe(req, res, next) {
  var Userlat = parseFloat(req.body.latitude);
  var Userlong = parseFloat(req.body.longitude);
  _ride2.default.find().then(function (doc) {
    var result = {
      object: doc
    };
    res.send(result);
  });
}

exports.default = {
  getRides: getRides,
  getRide: getRide,
  create: create,
  update: update,
  remove: remove,
  getRidesAroundMe: getRidesAroundMe
};
module.exports = exports['default'];
//# sourceMappingURL=ride.js.map
