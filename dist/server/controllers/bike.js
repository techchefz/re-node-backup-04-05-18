'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _bike2 = require('../models/bike');

var _bike3 = _interopRequireDefault(_bike2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function get(req, res) {
  return res.send({ success: true, message: 'bike found', data: req.bike });
}

function create(req, res, next) {
  _bike3.default.findOneAsync({
    $and: [{ bikeName: req.body.bikeName }, { bikeCategory: req.body.bikeCategory }]
  }).then(function (foundBike) {
    if (foundBike !== null) {
      var returnObj = {
        success: true,
        message: '',
        data: {}
      };
      returnObj.message = 'bike already exist';
      returnObj.success = false;
      return res.send(returnObj);
    } else {
      var _bike = new _bike3.default({
        bikeModel: req.body.bikeModel,
        bikeName: req.body.bikeName,
        bikeCategory: req.body.bikeCategory
      });
      _bike.saveAsync().then(function (savedBike) {
        var returnObj = {
          success: true,
          message: '',
          data: {}
        };
        returnObj.data.bike = savedBike;
        returnObj.message = 'bike created successfully';
        res.send(returnObj);
      }).error(function (e) {
        return next(e);
      });
    }
  });
}

function update(req, res, next) {
  //code to update bike details goes here ..
  _bike3.default.find({ _id: req.body._id }).then(function (doc) {
    doc[0].bikeModel = req.body.bikeModel ? req.body.bikeModel : doc[0].bikeModel, doc[0].bikeName = req.body.bikeName ? req.body.bikeName : doc[0].bikeName, doc[0].bikeCategory = req.body.bikeCategory ? req.body.bikeCategory : doc[0].bikeCategory;
  });
  bike.saveAsync().then(function (updatedBike) {
    var returnObj = {
      success: true,
      message: '',
      data: {}
    };
    returnObj.data.bike = updatedBike;
    returnObj.message = 'bike updated successfully';
    res.send(returnObj);
  }).error(function (e) {
    return next(e);
  });
}

function remove(req, res, next) {
  // code to remove bike goes here..
  _bike3.default.remove({ bikeName: req.body.bikeName }).then(function (removedBike) {
    var returnObj = {
      success: true,
      message: '',
      data: {}
    };
    returnObj.data.bike = removedBike;
    returnObj.message = 'bike removed successfully';
    res.send(returnObj);
  }).error(function (e) {
    return next(e);
  });
}

exports.default = {
  get: get,
  create: create,
  update: update,
  remove: remove
};
module.exports = exports['default'];
//# sourceMappingURL=bike.js.map
