'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _APIError = require('../helpers/APIError');

var _APIError2 = _interopRequireDefault(_APIError);

var _env = require('../../config/env');

var _env2 = _interopRequireDefault(_env);

var _user = require('../models/user');

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var crypto = require('crypto');

var _require = require('google-auth-library'),
    OAuth2Client = _require.OAuth2Client;

var google = require('googleapis');
var request = require('request');

/**
 * Returns jwt token  and user object if valid email and password is provided
 * @param req (email, password, userType)
 * @param res
 * @param next
 * @returns {jwtAccessToken, user}
 */
function login(req, res, next) {
  var retObj = {
    success: true,
    message: '',
    data: {}
  };

  var userObj = {
    email: req.body.email,
    userType: req.body.userType
  };

  _user2.default.findOneAsync(userObj, '+password').then(function (user) {
    //eslint-disable-line
    if (!user) {
      var err = new _APIError2.default('User not found with the given email id', _httpStatus2.default.NOT_FOUND);
      return next(err);
    } else {
      user.comparePassword(req.body.password, function (passwordError, isMatch) {
        //eslint-disable-line
        if (passwordError || !isMatch) {
          retObj.success = false;
          retObj.message = 'Incorrect Password';
          res.json(retObj);
        }
        user.loginStatus = true;
        var token = _jsonwebtoken2.default.sign(user, _env2.default.jwtSecret);
        _user2.default.findOneAndUpdateAsync({ _id: user._id }, { $set: user }, { new: true }) //eslint-disable-line
        .then(function (updatedUser) {
          var userObj = { email: updatedUser.email, profilePicture: { srcPath: updatedUser.profilePicture }, userId: updatedUser._id, phone: updatedUser.phoneNo, firstName: updatedUser.fname, lastName: updatedUser.lname };
          retObj.success = true;
          retObj.message = 'user successfully logged in';
          retObj.data.jwtAccessToken = 'JWT ' + token;
          retObj.data.user = userObj;
          res.json(retObj);
        }).error(function (err123) {
          var err = new _APIError2.default('error in updating user details while login ' + err123, _httpStatus2.default.INTERNAL_SERVER_ERROR);
          next(err);
        });
      });
    }
  }).error(function (e) {
    var err = new _APIError2.default('erro while finding user ' + e, _httpStatus2.default.INTERNAL_SERVER_ERROR);
    next(err);
  });
}

/** This is a protected route. Change login status to false and send success message.
 * @param req
 * @param res
 * @param next
 * @returns success message
 */

function logout(req, res, next) {
  var userObj = req.user;
  if (userObj === undefined || userObj === null) {
    console.log('user obj is null or undefined inside logout function'); //eslint-disable-line
  }
  userObj.loginStatus = false;
  _user2.default.findOneAndUpdate({ _id: userObj._id, loginStatus: true }, { $set: userObj }, { new: true }, function (err, userDoc) {
    //eslint-disable-line
    if (err) {
      var error = new _APIError2.default('error while updateing login status', _httpStatus2.default.INTERNAL_SERVER_ERROR);
      next(error);
    }
    if (userDoc) {
      var returnObj = {
        success: true,
        message: 'user logout successfully'
      };
      res.json(returnObj);
    } else {
      var _error = new _APIError2.default('user not found', _httpStatus2.default.NOT_FOUND);
      next(_error);
    }
  });
}

function checkUser(req, res) {
  _user2.default.findOneAsync({ email: req.body.email }).then(function (foundUser) {
    if (foundUser !== null) {
      var jwtAccessToken = _jsonwebtoken2.default.sign(foundUser, _env2.default.jwtSecret);
      var returnObj = {
        success: true,
        message: 'User Exist',
        data: {}
      };
      returnObj.data = {
        user: foundUser,
        jwtAccessToken: 'JWT ' + jwtAccessToken
      };
      return res.send(returnObj);
    } else {
      var _returnObj = {
        success: true,
        message: 'New User'
      };
      return res.send(_returnObj);
    }
  }).catch(function (error) {});
}

function facebookSignup(req, res, next) {
  var retObj = {
    status: false,
    userObj: null,
    message: null
  };

  var accessToken = req.body.AccessToken;
  var clientSecret = '7a73a87525b5af1c809058c73f42dca1';
  var appsecret_proof = crypto.createHmac('sha256', clientSecret).update(accessToken).digest('hex');
  var verifyUrl = 'https://graph.facebook.com/v2.12/me?fields=id,email,first_name,last_name,picture,gender&access_token=' + accessToken + '&appsecret_proof=' + appsecret_proof;
  request(verifyUrl, function (err, response, body) {
    var responseBody = JSON.parse(body);
    _user2.default.findOne({ email: responseBody.email }) //eslint-disable-line
    // eslint-disable-next-line
    .then(function (UserObj) {
      if (UserObj == null) {
        retObj.status = true, retObj.userObj = responseBody, retObj.message = null;
        res.send(retObj);
      } else if (UserObj.email === responseBody.email) {
        retObj.status = false;
        retObj.userObj = null;
        retObj.message = "User Already Exists !";
        res.send(retObj);
      } else {
        retObj.status = false;
        retObj.userObj = null;
        retObj.message = "Error Creating User, Please Try again !";
      }
    }, function (err) {
      console.log(err);
    });
  });
}

function googleSignup(req, res, next) {
  var retObj = {
    status: false,
    userObj: null,
    message: null
  };

  var ClientId = "800698914082-gshvmfmti62vt5ltm49ibgn7g2s43rkb.apps.googleusercontent.com";
  var ClientSecret = "Ux4kQpJp96pyxObSSaa6XK_2";

  function getOAuthClient() {
    return new OAuth2Client(ClientId, ClientSecret, "http://localhost:4502");
  }

  var oauth2Client = getOAuthClient();
  var code = req.body.authCode;
  oauth2Client.getToken(code, function (err, tokens) {
    // Now tokens contains an access_token and an optional refresh_token. Save them.
    if (!err) {
      oauth2Client.setCredentials(tokens);
      var token = tokens.id_token;
      var verifyUrl = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=' + token;
      request(verifyUrl, function (err, response, body) {
        var responseBody = JSON.parse(body);
        _user2.default.findOne({ email: responseBody.email }) //eslint-disable-line
        // eslint-disable-next-line
        .then(function (UserObj) {
          if (UserObj == null) {
            retObj.status = true, retObj.userObj = responseBody, retObj.message = null;
            res.send(retObj);
          } else if (UserObj.email === responseBody.email) {
            retObj.status = false;
            retObj.userObj = null;
            retObj.message = "User Already Exists !";
            res.send(retObj);
          } else {
            retObj.status = false;
            retObj.userObj = null;
            retObj.message = "Error Creating User, Please Try again !";
          }
        }, function (err) {
          console.log(err);
        });
      });
    } else {
      console.log(err);
    }
  });
}

function facebookLogin(req, res, next) {
  var retObj = {
    status: true,
    message: '',
    data: {}
  };
  var accessToken = req.body.obj.accessToken;
  var clientSecret = '7a73a87525b5af1c809058c73f42dca1';
  var appsecret_proof = crypto.createHmac('sha256', clientSecret).update(accessToken).digest('hex');
  var verifyUrl = 'https://graph.facebook.com/v2.12/me?fields=id,email,first_name,last_name,picture,gender&access_token=' + accessToken + '&appsecret_proof=' + appsecret_proof;
  request(verifyUrl, function (err, response, body) {
    var responseBody = JSON.parse(body);
    _user2.default.findOne({ email: responseBody.email }) //eslint-disable-line
    // eslint-disable-next-line
    .then(function (UserObj) {
      if (UserObj == null) {
        retObj.status = false;
        retObj.user = null;
        retObj.message = "User Doesn't Exists !";
        res.send(retObj);
      } else if (UserObj.email === responseBody.email) {
        retObj.status = true;
        retObj.message = "Logged in Successfully";
        var jwtAccessToken = _jsonwebtoken2.default.sign(UserObj, _env2.default.jwtSecret);
        retObj.data.jwtAccessToken = 'JWT ' + jwtAccessToken;
        var obj = { email: UserObj.email, profilePicture: { srcPath: UserObj.profilePicture }, userId: UserObj._id, phone: UserObj.phoneNo, firstName: UserObj.fname, lastName: UserObj.lname };
        retObj.data.user = obj;
        res.send(retObj);
      } else {
        retObj.status = false;
        retObj.user = null;
        retObj.message = "Error !, Please Try again !";
        res.send(retObj);
      }
    }, function (err) {
      console.log(err);
    });
  });
}

function googleLogin(req, res, next) {
  var retObj = {
    success: true,
    message: '',
    data: {}
  };

  var ClientId = "800698914082-gshvmfmti62vt5ltm49ibgn7g2s43rkb.apps.googleusercontent.com";
  var ClientSecret = "Ux4kQpJp96pyxObSSaa6XK_2";

  function getOAuthClient() {
    return new OAuth2Client(ClientId, ClientSecret, "http://localhost:4502");
  }

  var oauth2Client = getOAuthClient();
  var code = req.body.obj.authCode;
  oauth2Client.getToken(code, function (err, tokens) {
    // Now tokens contains an access_token and an optional refresh_token. Save them.
    if (!err) {
      oauth2Client.setCredentials(tokens);
      var token = tokens.id_token;
      var verifyUrl = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=' + token;
      request(verifyUrl, function (err, response, body) {
        var responseBody = JSON.parse(body);
        _user2.default.findOne({ email: responseBody.email }) //eslint-disable-line
        // eslint-disable-next-line
        .then(function (UserObj) {
          if (UserObj == null) {
            retObj.status = false;
            retObj.user = null;
            retObj.message = "User Doesn't Exists !";
            res.send(retObj);
          } else if (UserObj.email === responseBody.email) {
            retObj.status = true;
            retObj.message = "Logged in Successfully";
            var jwtAccessToken = _jsonwebtoken2.default.sign(UserObj, _env2.default.jwtSecret);
            retObj.data.jwtAccessToken = 'JWT ' + jwtAccessToken;
            var obj = { email: UserObj.email, profilePicture: { srcPath: UserObj.profilePicture }, userId: UserObj._id, phone: UserObj.phoneNo, firstName: UserObj.fname, lastName: UserObj.lname };
            retObj.data.user = obj;
            res.send(retObj);
          } else {
            retObj.status = false;
            retObj.user = null;
            retObj.message = "Error !, Please Try again !";
            res.send(retObj);
          }
        }, function (err) {
          console.log(err);
        });
      });
    } else {
      console.log(err);
    }
  });
}

exports.default = { login: login, logout: logout, checkUser: checkUser, facebookSignup: facebookSignup, googleSignup: googleSignup, googleLogin: googleLogin, facebookLogin: facebookLogin };
module.exports = exports['default'];
//# sourceMappingURL=auth.js.map
