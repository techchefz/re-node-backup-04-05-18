'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _reviews = require('../models/reviews');

var _reviews2 = _interopRequireDefault(_reviews);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function create(req, res, next) {
	var a = req.body.performance;

	var b = req.body.handling;
	var c = req.body.style;
	var d = req.body.ownershipexperience;
	var e = req.body.verstality;
	var sum = Number(a) + Number(b) + Number(c) + Number(d) + Number(e);
	var averageRating = sum / 5;
	console.log(averageRating);

	var averageRatingPercentage = averageRating * 20;
	console.log(averageRatingPercentage);
	console.log(req.body);
	var review = new _reviews2.default({
		reviewEntityId: req.body.reviewEntityId,

		reviewByUser: req.body.reviewByUser,
		reviewDescription: req.body.reviewDescription,
		reviewText: req.body.reviewText,
		// reviewDateText: req.body.reviewDateText,
		averageRating: averageRating,
		averageRatingPercentage: averageRatingPercentage,
		reviewCriterias: {

			performance: {
				review: {
					reviewCriteria: 'performance',
					reviewCriteriaLabel: 'Performance',
					averageRating: req.body.performance,
					averagePercentage: req.body.performance * 20
				}
			},
			handling: {
				review: {
					reviewCriteria: 'handling',
					reviewCriteriaLabel: 'Handling',
					averageRating: req.body.handling,
					averagePercentage: req.body.handling * 20
				}
			},
			style: {
				review: {
					reviewCriteria: 'style',
					reviewCriteriaLabel: 'Style',
					averageRating: req.body.style,
					averagePercentage: req.body.style * 20
				}
			},
			verstality: {
				review: {
					reviewCriteria: 'verstality',
					reviewCriteriaLabel: 'Verstality',
					averageRating: req.body.verstality,
					averagePercentage: req.body.verstality * 20
				}
			},
			ownershipExperience: {
				review: {
					reviewCriteria: 'ownershipexperience',
					reviewCriteriaLabel: 'OwnershipExperience',
					averageRating: req.body.ownershipexperience,
					averagePercentage: req.body.ownershipexperience * 20
				}
			}
		}

	});

	review.save().then(function (doc) {
		console.log(doc);
		res.send(doc);
	}, function (err) {
		res.send(err);
	});
}

// for  detial listing-------------------------------------------------------------------------------------
function fetch(req, res, next) {
	_reviews2.default.aggregate([{ $match: { reviewEntityId: 'ContinentalGT' } }, {
		$group: {
			_id: 'reviewEntityId',
			Count: { $sum: 1 },
			averagePerformance: { $avg: '$reviewCriterias.performance.review.averageRating' },
			averagehandling: { $avg: '$reviewCriterias.handling.review.averageRating' },
			averageownershipExperience: { $avg: '$reviewCriterias.ownershipExperience.review.averageRating' },
			averageverstality: { $avg: '$reviewCriterias.verstality.review.averageRating' },
			averagestyle: { $avg: '$reviewCriterias.style.review.averageRating' }
		}
	}]).then(function (doc) {

		console.log(doc[0]);

		var performancerating = doc[0].averagePerformance;
		var performanceratingpercentage = performancerating * 20;

		//    //	------------------------
		var stylerating = doc[0].averagestyle;
		var stylepercentage = stylerating * 20;

		//    //---------------------------------	
		var verstalityrating = doc[0].averageverstality;
		var verstalityratingpercentage = verstalityrating * 20;

		//    	//----------------------------
		var ownershipExperiencerating = doc[0].averageownershipExperience;
		var ownershipExperiencepercentage = ownershipExperiencerating * 20;

		//    	// ----------------------------------
		var handlingrating = doc[0].averagehandling;
		var handlingpercentage = handlingrating * 20;

		// //----------------------------------------   	
		var count = doc[0].Count;
		var averageRating = (doc[0].averagePerformance + doc[0].averagestyle + doc[0].averageverstality + doc[0].averageownershipExperience + doc[0].averagehandling) / 5;
		var averageRatingPercentage = averageRating / 5 * 100;
		console.log(averageRating);
		console.log(averageRatingPercentage);
		// //------------------------------------

		var obj = {};
		obj = {
			// req.body.EntityId
			reviewEntityId: 'ContinentalGT',
			averageRating: averageRating,
			averageRatingPercentage: averageRatingPercentage,
			reviewersCount: count,
			reviewCriterias: {

				performance: {
					review: {
						reviewCriteria: 'performane',
						reviewCriteriaLabel: 'Performance',
						reviewRating: performancerating,
						reviewRatingPercentage: performanceratingpercentage
					}
				},

				handling: {

					review: {
						reviewCriteria: 'handling',
						reviewCriteriaLabel: 'Handling',
						reviewRating: handlingrating,
						reviewRatingPercentage: handlingpercentage

					}
				},
				style: {

					review: {
						reviewCriteria: 'style',
						reviewCriteriaLabel: 'Style',
						reviewRating: stylerating,
						reviewRatingPercentage: stylepercentage

					}
				},

				verstality: {

					review: {
						reviewCriteria: 'verstality',
						reviewCriteriaLabel: 'Verstality',
						reviewRating: verstalityrating,
						reviewRatingPercentage: verstalityratingpercentage
					}
				},

				ownershipExperience: {

					review: {
						reviewCriteria: 'ownershipExperience',
						reviewCriteriaLabel: 'OwnershipExperience',
						reviewRating: ownershipExperiencerating,
						reviewRatingPercentage: ownershipExperiencepercentage

					}
				}
			}
		};
		console.log(obj);

		//--------------------------------------  	

		_reviews2.default.find({ reviewEntityId: 'ContinentalGT' }, { averageRatingPercentage: 1, averageRating: 1, reviewCriterias: 1, _id: 0, reviewText: 1, reviewDescription: 1, reviewDateText: 1 }).populate('reviewByUser', 'fname lname profilePicture -_id').then(function (docs) {

			var array = [];

			var i = 0;
			for (i = 0; i < docs.length; i++) {
				array.push(_defineProperty({
					reviewText: docs[i].reviewText,
					averageRatingPercentage: docs[i].averageRatingPercentage,
					reviewByUser: { firstName: docs[i].reviewByUser.fname, profilePicture: { srcPath: docs[i].reviewByUser.profilePicture } },
					reviewCriterias: docs[i].reviewCriterias,
					averageRating: docs[i].averageRating,
					reviewDescription: docs[i].reviewDescription

				}, 'reviewText', docs[i].reviewText));
			}

			obj.reviewList = array;
			console.log(obj);
			res.send(obj);
		});
	});
}

//-------------------------------------------------------------------------------------------------
// summary details reviews

function getReviews(req, res, next) {

	var reviewEntityId = req.query.jsonString;

	_reviews2.default.find({ reviewEntityId: 'ContinentalGT' }, { reviewText: 1, _id: 0, averageRatingPercentage: 1 }).populate('reviewByUser', 'fname lname profilePicture').then(function (doc) {
		console.log(doc);
		var obj = [];
		var sum = 0;
		var i = 0;
		console.log(req.query.jsonString);

		for (i = 0; i < doc.length; i++) {

			obj.push({
				reviewText: doc[i].reviewText,
				averageRatingPercentage: doc[i].averageRatingPercentage,
				reviewByUser: { firstName: doc[i].reviewByUser.fname, profilePicture: { srcPath: doc[i].reviewByUser.profilePicture } }

			});

			sum = sum + doc[i].averageRatingPercentage;
		}
		var finalRating = sum / doc.length;

		res.send({ reviewersCount: doc.length, averageRatingPercentage: finalRating, reviewList: obj });
	});
}

exports.default = { create: create, fetch: fetch, getReviews: getReviews };
module.exports = exports['default'];
//# sourceMappingURL=reviews.js.map
