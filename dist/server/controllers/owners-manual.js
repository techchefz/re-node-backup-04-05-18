'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _ownersManual = require('../models/owners-manual');

var _ownersManual2 = _interopRequireDefault(_ownersManual);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function create(req, res, next) {

    var ownersManual = new _ownersManual2.default({

        name: req.body.name,
        email: req.body.email,
        phone: req.body.phone,
        city: req.body.city

    });

    ownersManual.saveAsync().then(function (doc) {
        res.send('saved successfully');
    });
}

exports.default = {
    create: create
};
module.exports = exports['default'];
//# sourceMappingURL=owners-manual.js.map
