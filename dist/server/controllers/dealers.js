'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _dealers = require('../models/dealers');

var _dealers2 = _interopRequireDefault(_dealers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function get(req, res) {
  // connect to mongodb and find list of dealers and return the response
  return res.send({ success: true, message: 'dealer found', data: req.dealers });
}

function create(req, res, next) {
  _dealers2.default.findOneAsync({
    $and: [{ dealerName: req.body.dealerName }, { placeId: req.body.placeId }]
  }).then(function (foundDealer) {
    if (foundDealer !== null) {
      var returnObj = {
        success: true,
        message: '',
        data: {}
      };
      returnObj.message = 'dealer already exist';
      returnObj.success = false;
      return res.send(returnObj);
    } else {
      var dealer = new _dealers2.default({

        dealer_id: req.body.dealer_id,
        DealerName: req.body.DealerName,
        BranchName: req.body.BranchName,
        address: req.body.address,
        contact_number: req.body.contact_number,
        isServiceApplicable: req.body.isServiceApplicable,
        isSalesApplicable: req.body.isSalesApplicable,
        StartTime: req.body.StartTime,
        EndTime: req.body.EndTime,
        Week_Off: req.body.Week_Off,
        image: req.body.image,
        email: req.body.email,
        review: req.body.review
      });
      dealer.saveAsync().then(function (savedDealer) {
        var returnObj = {
          success: true,
          message: '',
          data: {}
        };
        returnObj.data.dealer = savedDealer;
        returnObj.message = 'dealer created successfully';
        res.send(returnObj);
      }).error(function (e) {
        return next(e);
      });
    }
  });
}
function update(req, res, next) {
  //code to update dealer details goes here ..
}

function remove(req, res, next) {
  // code to remove dealer goes here..
}

exports.default = {
  get: get,
  create: create,
  update: update,
  remove: remove
};
module.exports = exports['default'];
//# sourceMappingURL=dealers.js.map
