'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _citiesStates = require('../models/citiesStates');

var _citiesStates2 = _interopRequireDefault(_citiesStates);

var _dealers = require('../models/dealers');

var _dealers2 = _interopRequireDefault(_dealers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var headers = {
    'Content-Type': 'text/plain'
};

var fetchCityStatesData = {
    "ResourceIdentifier": 206,
    "EntityType": "9021",
    "EntityID": 45,
    "CardType": 34,
    RequestKey: "kjwkdjsddcawdwhcb"
};

var fetchCityStatesString = JSON.stringify(fetchCityStatesData);

var fetchCityStatesOptions = {
    url: 'http://refineb2cdvp.excelloncloud.com/exapis//api/portalentity',
    method: 'POST',
    headers: headers,
    body: fetchCityStatesString
};

function fetchCityStates(req, res, next) {

    request(fetchCityStatesOptions, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            // Print out the response body
            console.log(body);
            res.send(body);
            var cityStatesData = JSON.parse(body);
            _citiesStates2.default.insertMany(cityStatesData, function (doc) {
                res.send(doc);
            });
        } else {
            console.log(error);
        }
    });
}

function getcity(req, res, next) {
    _citiesStates2.default.find({ StateName: req.body.stateName }).then(function (doc) {
        var citiesArr = [];
        doc.map(function (a) {
            citiesArr.push(a.CityName);
        });
        res.send(citiesArr);
    });
}

function getState(req, res, next) {
    _citiesStates2.default.distinct('StateCode').then(function (doc) {
        res.send(doc);
        console.log(doc);
    });
}

function getDealers(req, res, next) {

    _dealers2.default.find({ city: req.body.city }).then(function (doc) {
        var dealersArr = [];
        doc.map(function (dealer) {
            var dealerObj = {
                "DealerName": dealer.DealerName,
                "BranchName": dealer.BranchName,
                "address": dealer.address,
                "BranchID": dealer.BranchID
            };
            dealersArr.push(dealerObj);
        });
        res.send(dealersArr);
    });
}
exports.default = {
    getState: getState,
    getcity: getcity,
    getDealers: getDealers
};
module.exports = exports['default'];
//# sourceMappingURL=cityStates.js.map
