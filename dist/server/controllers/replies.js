'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _comments = require('../models/comments');

var _comments2 = _interopRequireDefault(_comments);

var _replies = require('../models/replies');

var _replies2 = _interopRequireDefault(_replies);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function createReplies(req, res, next) {
    var id = req.body.commentid;
    var userid = req.body.userid;
    var replyBody = req.body.replyBody;
    console.log("-------------------------------------------------------------------------------------------------------------");
    console.log(req.body);
    _comments2.default.find({ _id: id }).then(function (commentsarray) {
        var reply = new _replies2.default({
            replyBody: replyBody,
            userdetailsreplies: userid
        });

        reply.save().then(function (doc) {
            commentsarray[0].Replyid.push(doc._id);
            commentsarray[0].save().then(function (details) {
                res.send("replysaved");
            });
        });
    });
}

exports.default = { createReplies: createReplies };
module.exports = exports['default'];
//# sourceMappingURL=replies.js.map
