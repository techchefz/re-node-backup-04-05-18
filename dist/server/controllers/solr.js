'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _solrClient = require('solr-client');

var _solrClient2 = _interopRequireDefault(_solrClient);

var _env = require('../../config/env');

var _env2 = _interopRequireDefault(_env);

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var indexer = _solrClient2.default.createClient({
    host: _env2.default.solr,
    port: 8983,
    path: "/solr",
    core: "new_core",
    solrVersion: 721
});

function searchSolr(req, res, next) {

    var query2 = indexer.createQuery().q('body: ' + req.query.query + ' OR title: ' + req.query.query)
    //.q(`name: ${req.query.query}`)
    .start(0).rows(10).fl('url,title');
    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err);
        } else {

            res.send(obj);
        }
    });
}

function sendSms(req, res, next) {
    console.log('Running');
    var phoneNo = 9599914762;
    var verifyUrl = 'https://push3.maccesssmspush.com/servlet/com.aclwireless.pushconnectivity.listeners.TextListener?userId=demoacl3&pass=demoacl12&appid=demoacl3&subappid=demoacl3&contenttype=1&to=9599914762&from=DEMOAW&text=testsmsbody&selfid=true&alert=1&dlrreq=true';
    (0, _request2.default)(verifyUrl, function (err, response, body) {
        console.log(err);
        console.log(body);
        console.log(response);
        res.send(body);
    });
}

function searchRide(req, res, next) {

    var phoneNo = 9999594606;
    var smsText = 1234;
    var verifyUrl = 'https://push3.maccesssmspush.com/servlet/com.aclwireless.pushconnectivity.listeners.TextListener?userId=demoacl3&pass=demoacl12&appid=demoacl3&subappid=demoacl3&contenttype=1&to=9599914762&from=DEMOAW&text=testsmsbody&selfid=true&alert=1&dlrreq=true';
    (0, _request2.default)(verifyUrl, function (err, response, body) {
        console.log(err);
        console.log(body);
        console.log(response);
        res.send("sdad" + body);
    });
}

function tripStorySearch(req, res, next) {
    var resultFromSolr = [];
    var i = 0;
    var query2 = indexer.createQuery().q('entity: \'trip-story\' && (category : ' + req.body.category + ' OR title : ' + req.body.category + ')  ').start(0).rows(10).fl('category,title,author,thumbnailImagePath,summary,url');

    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err);
        } else {
            console.log(obj);

            for (i = 0; i < obj.response.docs.length; i++) {
                resultFromSolr.push({ createdBy: obj.response.docs[i].author[0], category: obj.response.docs[i].category[0], title: obj.response.docs[i].title[0], url: obj.response.docs[i].url[0], summary: obj.response.docs[i].summary[0], rideImage: obj.response.docs[i].thumbnailImagePath[0] });
            }

            res.send({ objects: resultFromSolr });
        }
    });
}

function tripStorySearchOnClick(req, res, next) {
    var i = 0;
    var resultFromSolr = [];
    var query2 = indexer.createQuery().q('entity: \'trip-story\' && category: ' + req.body.category).start(0).rows(10).fl('category,title,summary,thumbnailImagePath,author,url');
    var request = indexer.search(query2, function (err, obj) {
        if (err) {
            res.send(err);
        } else {
            console.log(obj);
            for (i = 0; i < obj.response.docs.length; i++) {
                resultFromSolr.push({ createdBy: obj.response.docs[i].author[0], category: obj.response.docs[i].category[0], summary: obj.response.docs[i].summary[0], title: obj.response.docs[i].title[0], url: obj.response.docs[i].url[0], rideImage: obj.response.docs[i].thumbnailImagePath[0] });
            }
            res.send({ objects: resultFromSolr });
        }
    });
}

exports.default = { sendSms: sendSms, searchSolr: searchSolr, searchRide: searchRide, tripStorySearch: tripStorySearch, tripStorySearchOnClick: tripStorySearchOnClick };
module.exports = exports['default'];
//# sourceMappingURL=solr.js.map
