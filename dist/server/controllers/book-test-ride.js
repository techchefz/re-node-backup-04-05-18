"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _bookTestRide = require("../models/book-test-ride");

var _bookTestRide2 = _interopRequireDefault(_bookTestRide);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function create(req, res, next) {
    var bookTestRide = new _bookTestRide2.default({
        fName: req.body.fName,
        lName: req.body.lName,
        email: req.body.email,
        bikeName: req.body.bikeName,
        countryName: req.body.countryName,
        stateName: req.body.stateName,
        cityName: req.body.cityName,
        dealerName: req.body.dealerName,
        Date: req.body.Date,
        mobile: req.body.mobile
    });
    bookTestRide.saveAsync().then(function (savedTestRide) {
        res.send("Test Ride booked successfully");
    });
};

exports.default = {
    create: create
};
module.exports = exports["default"];
//# sourceMappingURL=book-test-ride.js.map
