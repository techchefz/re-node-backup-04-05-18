'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _dealers = require('../models/dealers');

var _dealers2 = _interopRequireDefault(_dealers);

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _user = require('../models/user');

var _user2 = _interopRequireDefault(_user);

var _env = require('../../config/env');

var _env2 = _interopRequireDefault(_env);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getRandomInt() {
    var number = Math.floor(Math.random() * Math.floor(10000));
    return number.toString();
}
var random = 0;

var headers = {
    'Content-Type': 'text/plain'
};

var dealersData = {
    'ResourceIdentifier': 206,
    'EntityType': "9002",
    'LastModifiedDateTime': "",
    'RequestKey': getRandomInt()
};
var dealers = JSON.stringify(dealersData);

var dealersOptions = {
    url: 'http://refineb2cdvp.excelloncloud.com/exapis//api/portalentity',
    method: 'POST',
    headers: headers,
    body: dealers
};

function getDealers(req, res, next) {

    (0, _request2.default)(dealersOptions, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            // Print out the response body
            var dealerRequestData = JSON.parse(body).CompanyMaster_portal;
            _dealers2.default.insertMany(dealerRequestData, function () {
                res.send("Inserted Successfully");
            });
        } else {
            console.log(error);
        }
    });
}

function findPincode(req, res, next) {

    _dealers2.default.find({ Pincode: req.body.pinCode }).then(function (foundPincode) {

        res.send(foundPincode);
    });
}

function findState(req, res, next) {

    _dealers2.default.find({ State: req.body.state }).then(function (foundState) {

        res.send(foundState);
    });
}

function findCity(req, res, next) {

    _dealers2.default.find({ City: req.body.city }).then(function (foundCity) {

        res.send(foundCity);
    });
}

function getBikeDetail(req, res, next) {

    var bikeDetailData = {
        ResourceIdentifier: 206,
        EntityType: "9021",
        EntityID: 53,
        RegistrationNo: "",
        ChassisNo: "",
        EngineNo: "",
        MobileNo: req.body.phoneNo,
        RequestKey: getRandomInt()
    };

    console.log(bikeDetailData);
    var bikeDetail = JSON.stringify(bikeDetailData);

    var bikeDetailOptions = {
        url: '' + _env2.default.dmsApiUrl,
        method: 'POST',
        headers: headers,
        body: bikeDetail
    };

    (0, _request2.default)(bikeDetailOptions, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            // Print out the response body
            res.send(body);
        } else {
            console.log(error);
        }
    });
}

function getSlots(req, res, next) {
    console.log('Random Number :' + getRandomInt());
    var slotsData = {
        "ResourceIdentifier": 206,
        "EntityType": "9027",
        "BranchID": req.body.BranchID,
        "AppointmentDate": req.body.AppointmentDate,
        "RequestKey": getRandomInt()
    };

    var slots = JSON.stringify(slotsData);
    var slotsOptions = {
        url: "http://refineb2cdvp.excelloncloud.com/exapis//api/portalentity",
        method: 'POST',
        headers: headers,
        body: slots
    };
    (0, _request2.default)(slotsOptions, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            // Print out the response body
            var myData = JSON.parse(body);
            var arr = [];

            for (var i = 0; i < myData.length; i++) {
                arr.push({ startTime: myData[i].SlotFromTime, endTime: myData[i].SlotToTime, CompanyID: myData[i].CompanyID, SlotID: myData[i].SlotID, BranchID: myData[i].BranchID });
            }
            console.log('success');
            res.send(arr);
        } else {
            console.log(error);
        }
    });
}

function serviceBooking(req, res, next) {
    var serviceBookingData = {
        "ResourceIdentifier": 204,
        "EntityType": 9000,
        FirstName: req.body.FirstName,
        LastName: req.body.LastName,
        Mobile: req.body.Mobile,
        Email: req.body.Email,
        Gender: req.body.Gender,
        HomeAddress: req.body.HomeAddress,
        OfficeAddress: req.body.OfficeAddress,
        CityName: req.body.City,
        StateName: req.body.StateName,
        CountryName: req.body.CountryName,
        "UserID": req.body.UserID,
        "ServiceType": req.body.ServiceType,
        "RegNo": req.body.RegNo,
        "IsPickUp": 1,
        "IsDrop": 1,
        "PickUpAddress": req.body.PickUpAddress,
        "DropAddress": req.body.DropAddress,
        "AppointmentDate": req.body.AppointmentDate,
        "CustomerRemarks": req.body.CustomerRemarks,
        "SlotID": req.body.SlotID,
        "Attachment": '',
        "CompanyID": req.body.CompanyID,
        "BranchID": req.body.BranchID,
        "source": "W",
        requestKey: getRandomInt()
    };

    var serviceBooking = JSON.stringify(serviceBookingData);

    var serviceBookingOptions = {
        url: '' + _env2.default.dmsApiUrl,
        method: 'POST',
        headers: headers,
        body: serviceBooking
    };

    (0, _request2.default)(serviceBookingOptions, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            // Print out the response body
            res.send(body);
        } else {
            console.log(error);
        }
    });
}

function getDates(req, res, next) {

    var presentDate = (0, _moment2.default)().format('YYYY-MM-DD');
    var day1 = (0, _moment2.default)().add(1, 'days');
    var day2 = (0, _moment2.default)().add(2, 'days');
    var day3 = (0, _moment2.default)().add(3, 'days');
    var day4 = (0, _moment2.default)().add(4, 'days');
    var day5 = (0, _moment2.default)().add(5, 'days');
    var day6 = (0, _moment2.default)().add(6, 'days');
    var day7 = (0, _moment2.default)().add(7, 'days');

    var day01 = presentDate;
    var day02 = day1.format('YYYY-MM-DD');
    var day03 = day2.format('YYYY-MM-DD');
    var day04 = day3.format('YYYY-MM-DD');
    var day05 = day4.format('YYYY-MM-DD');
    var day06 = day5.format('YYYY-MM-DD');
    var day07 = day6.format('YYYY-MM-DD');
    var day08 = day7.format('YYYY-MM-DD');

    var day1_name = day1.day();
    var day2_name = day2.day();
    var day3_name = day3.day();
    var day4_name = day4.day();
    var day5_name = day5.day();
    var day6_name = day6.day();
    var day7_name = day7.day();

    var theday1 = "TODAY";
    var theday2 = "TOMMORROW";
    var theday3 = getmyday(day2_name);
    var theday4 = getmyday(day3_name);
    var theday5 = getmyday(day4_name);
    var theday6 = getmyday(day5_name);
    var theday7 = getmyday(day6_name);

    var arr = [{ date: day01, day: theday1 }, { date: day02, day: theday2 }, { date: day03, day: theday3 }, { date: day04, day: theday4 }, { date: day05, day: theday5 }, { date: day06, day: theday6 }, { date: day07, day: theday7 }];
    res.send(arr);
}

function getmyday(a) {
    if (a == 1) {
        return "MONDAY";
    } else if (a == 2) {
        return "TUESDAY";
    } else if (a == 3) {
        return "WEDNESDAY";
    } else if (a == 4) {
        return "THURSDAY";
    } else if (a == 5) {
        return "FRIDAY";
    } else if (a == 6) {
        return 'SATURDAY';
    } else if (a == 0) {
        return 'SUNDAY';
    }
}

function sendVerificationSms(req, res, next) {
    var phoneNo = req.body.phoneNo;
    console.log(req.body);
    _user2.default.find({ phoneNo: phoneNo }).then(function (user) {
        if (user && user.length > 0) {
            var otpValue = Math.floor(100000 + Math.random() * 900000);
            user[0].otp = otpValue;
            user[0].save();
            _axios2.default.get('http://www.sms.wstechno.com/ComposeSMS.aspx?username=Isetsol-170841&password=Wsc@4141@&sender=WSTECH&&to=' + phoneNo + '&message=Please Use this OTP for Service Booking.OTP is :' + otpValue + '&priority=1&dnd=1&unicode=0').then(function (response) {
                console.log("otp has been sent");
            }).catch(function (error) {
                console.log(error);
                console.log("Error in sending sms");
            });
            res.send('otp has been sent');
        } else {
            res.send("User doesnt exist");
        }
    });
}

exports.default = {
    getDealers: getDealers,
    findPincode: findPincode,
    findState: findState,
    findCity: findCity,
    getBikeDetail: getBikeDetail,
    getSlots: getSlots,
    serviceBooking: serviceBooking,
    getDates: getDates,
    sendVerificationSms: sendVerificationSms
};
module.exports = exports['default'];
//# sourceMappingURL=serviceBooking.js.map
