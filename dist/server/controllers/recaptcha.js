'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _recaptcha = require('../models/recaptcha');

var _recaptcha2 = _interopRequireDefault(_recaptcha);

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function recaptchastore(req, res, next) {
  req.on("data", function (buffer) {
    var obj = JSON.parse(buffer);
    var randomnumber = obj.randomnumber;
    var publickey = obj.publickey;
    var privatekey = obj.privatekey;

    var recaptcha = new _recaptcha2.default({
      Randomnumber: randomnumber,
      publickey: publickey,
      privatekey: privatekey
    });

    recaptcha.save().then(function (doc) {
      console.log(doc);
    }, function (e) {
      console.log(e);
    });

    res.send('asdasdsa');
  });
}

function recaptchaverify(req, res, next) {
  if (req.body.captcha === undefined || req.body.captcha === '' || req.body.captcha === null) {
    return res.json({ "success": false, "msg": "Please select captcha" });
  }

  var randomnumber = req.body.randomnumber;
  var publickey = req.body.publickey;

  _recaptcha2.default.find({ randomnumber: randomnumber }).then(function (todos) {
    console.log(todos);
  }, function (e) {
    console.log(e);
  });

  //Secret Key
  var secretKey = '6Le9V0QUAAAAADlrF3TzTyVy8mPhmd-vgOqN5RyL';

  // Verify URL
  var verifyUrl = 'https://google.com/recaptcha/api/siteverify?secret=' + secretKey + '&response=' + req.body.captcha + '&remoteip=' + req.connection.remoteAddress;

  // Make Request To VerifyURL
  (0, _request2.default)(verifyUrl, function (err, response, body) {
    body = JSON.parse(body);
    console.log(body);

    // If Not Successful
    if (body.success !== undefined && !body.success) {
      console.log('invalid private key');
    }
  });
}

exports.default = {

  recaptchaverify: recaptchaverify,
  recaptchastore: recaptchastore

};
module.exports = exports['default'];
//# sourceMappingURL=recaptcha.js.map
