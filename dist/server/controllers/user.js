'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _bcrypt = require('bcrypt');

var _bcrypt2 = _interopRequireDefault(_bcrypt);

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _formidable = require('formidable');

var _formidable2 = _interopRequireDefault(_formidable);

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _APIError = require('../helpers/APIError');

var _APIError2 = _interopRequireDefault(_APIError);

var _env = require('../../config/env');

var _env2 = _interopRequireDefault(_env);

var _user = require('../models/user');

var _user2 = _interopRequireDefault(_user);

var _emailApi = require('../service/emailApi');

var _emailApi2 = _interopRequireDefault(_emailApi);

var _smsApi = require('../service/smsApi');

var _smsApi2 = _interopRequireDefault(_smsApi);

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Get user
 * @returns {User}
 */

function get(req, res) {
  return res.send({ success: true, message: 'user found', data: req.user });
}

/**
 * Create new user
 * @property {string} req.body.username - The username of user.
 * @property {string} req.body.mobileNumber - The mobileNumber of user.
 * @returns {User}
 */

function create(req, res, next) {
  _user2.default.findOneAsync({
    $or: [{ $and: [{ email: req.body.email }, { phoneNo: req.body.phoneNo }] }, { $or: [{ email: req.body.email }, { phoneNo: req.body.phoneNo }] }]
  }).then(function (foundUser) {
    if (foundUser !== null && foundUser.userType === req.body.userType) {
      _user2.default.findOneAndUpdateAsync({ _id: foundUser._id }, { $set: { loginStatus: true } }, { new: true }) //eslint-disable-line
      // eslint-disable-next-line
      .then(function (updateUserObj) {
        if (updateUserObj) {
          var jwtAccessToken = _jsonwebtoken2.default.sign(updateUserObj, _env2.default.jwtSecret);
          var _returnObj = {
            success: true,
            message: '',
            data: {}
          };
          _returnObj.message = 'User Already Exists !';
          _returnObj.success = false;
          return res.send(_returnObj);
        }
      }).error(function (e) {
        var err = new _APIError2.default('error in updating user details while login ' + e, _httpStatus2.default.INTERNAL_SERVER_ERROR);
        next(err);
      });
    } else {
      var otpValue = Math.floor(100000 + Math.random() * 900000); //eslint-disable-line
      var emailToken = Math.floor(700000000000 + Math.random() * 900000); //eslint-disable-line
      var user = new _user2.default({
        email: req.body.email,
        password: req.body.password,
        userType: req.body.userType,
        fname: req.body.fname,
        lname: req.body.lname,
        bikeName: req.body.bikeName,
        ownBike: req.body.ownBike,
        phoneNo: req.body.phoneNo,
        otp: otpValue,
        emailToken: emailToken,
        loginStatus: true,
        addressInfo: {
          city: req.body.city
        }
      });
      user.saveAsync().then(function (savedUser) {
        console.log(savedUser);
        var username = 'admin';
        var password = 'admin';
        var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
        _request2.default.post({
          headers: {
            'content-type': 'application/x-www-form-urlencoded',
            "Authorization": auth
          },
          url: _env2.default.aemAuthorUrl + '/bin/createPage',
          form: {
            entity: 'user',
            pageId: savedUser._id.toString(),
            title: savedUser.fname.toString()

          }
        }, function (error, response, dataReturned) {

          var data = JSON.parse(dataReturned);
          if (error) {
            returnObj.data.story = null;
            returnObj.message = 'Error Creating User';
            returnObj.success = false;
            res.send(returnObj);
          } else {
            console.log(dataReturned);
            var _returnObj2 = {
              success: true,
              message: '',
              data: {}
            };
            _user2.default.findOneAndUpdate({ _id: savedUser._id }, { $set: { userUrl: dataReturned.pagePath } }).then(function (updatedUser) {
              var jwtAccessToken = _jsonwebtoken2.default.sign(savedUser, _env2.default.jwtSecret);
              _returnObj2.data.jwtAccessToken = 'JWT ' + jwtAccessToken;
              var obj = { email: savedUser.email, profilePicture: { srcPath: savedUser.profilePicture }, userId: savedUser._id, phone: savedUser.phoneNo, firstName: savedUser.fname, lastName: savedUser.lname };
              _returnObj2.data.user = obj;
              _returnObj2.message = 'user created successfully';
              res.send(_returnObj2);
            });
          }
        });
      }).error(function (e) {
        return next(e);
      });
    }
  });
}

function sendSms(smsText, phoneNo) {
  var num = phoneNo.slice(-10);
  _axios2.default.get('http://www.sms.wstechno.com/ComposeSMS.aspx?username=Isetsol-170841&password=Wsc@4141@&sender=WSTECH&&to=' + num + '&message=Please Use this OTP for resetting the password for your Royal Enfield Account.OTP is :' + smsText + '&priority=1&dnd=1&unicode=0').then(function (response) {
    console.log('otp has been sent');
  }).catch(function (error) {
    console.log(error);
    console.log("Error in sending sms");
  });
}

function forgot(req, res, next) {
  var phoneNo = req.body.phoneNo;
  _user2.default.find({ phoneNo: phoneNo }).then(function (user) {
    if (user && user.length > 0) {
      var otpValue = Math.floor(100000 + Math.random() * 900000);
      user[0].otp = otpValue;
      user[0].save();
      sendSms(otpValue, phoneNo);
      res.send("otp has been sent");
    } else {
      res.send("User doesnt exist");
    }
  });
}

/**
 * Update existing user
 * @property {Object} req.body.user - user object containing all fields.
 * @returns {User}
 */
function update(req, res, next) {
  var user = req.user;
  user.fname = req.body.fname ? req.body.fname : user.fname;
  user.lname = req.body.lname ? req.body.lname : user.lname;
  user.email = req.body.email ? req.body.email : user.email;
  user.dob = req.body.dob ? req.body.dob : user.dob;
  user.gender = req.body.gender ? req.body.gender : user.gender;
  user.phoneNo = req.body.phoneNo ? req.body.phoneNo : user.phoneNo;
  user.homeAddressInfo.address = req.body.homeAddress ? req.body.homeAddress : user.homeAddressInfo.address;
  user.homeAddressInfo.city = req.body.city ? req.body.city : user.homeAddressInfo.city;
  user.homeAddressInfo.state = req.body.state ? req.body.state : user.homeAddressInfo.state;
  user.homeAddressInfo.country = req.body.country ? req.body.country : user.homeAddressInfo.country;
  user.homeAddressInfo.streetLocalityName = req.body.streetLocalityName ? req.body.streetLocalityName : user.streetLocalityName;
  user.homeAddressInfo.buildingNameFlatNo = req.body.buildingNameFlatNo ? req.body.buildingNameFlatNo : user.buildingNameFlatNo;
  user.homeAddressInfo.Landmark = req.body.Landmark ? req.body.Landmark : user.Landmark;
  user.officeAddressInfo.address = req.body.officeAddress ? req.body.officeAddress : user.officeAddressInfo.address;
  user.bikeOwned = req.body.bikeOwned ? req.body.bikeOwned : user.bikeOwned;
  user.favouriteQuote = req.body.favouriteQuote ? req.body.favouriteQuote : user.favouriteQuote;
  user.favouriteRideExperience = req.body.favouriteRideExperience ? req.body.favouriteRideExperience : user.favouriteRideExperience;
  user.saveAsync().then(function (savedUser) {

    var userDetailEditData = {
      ResourceIdentifier: 204,
      EntityType: 9011,
      EntityId: 24,
      FirstName: savedUser.fname,
      LastName: savedUser.lname,
      DateofBirth: savedUser.dob,
      MobileNo: savedUser.phoneNo,
      EmailID: savedUser.email,
      Gender: savedUser.gender,
      HomeAddress: savedUser.homeAddressInfo.address,
      OfficeAddress: savedUser.officeAddressInfo.address,
      CityName: savedUser.homeAddressInfo.city,
      StateName: savedUser.homeAddressInfo.state,
      CountryName: savedUser.homeAddressInfo.country,
      StreetLocalityName: savedUser.homeAddressInfo.streetLocalityName,
      BuildingNameFlatNo: savedUser.homeAddressInfo.buildingNameFlatNo,
      Landmark: savedUser.homeAddressInfo.Landmark
    };

    var userDetailEdit = JSON.stringify(userDetailData);

    var userDetailEditOptions = {
      url: '' + _env2.default.dmsApiUrl,
      method: 'POST',
      headers: 'text/plain',
      body: userDetailEdit
    };

    var headers = {
      'Content-Type': 'text/plain'
    };

    (0, _request2.default)(userDetailEditOptions, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        // Print out the response body
        console.log(body);
        res.send("successfully saved");
      } else {
        console.log(error);
      }
    });
  }).error(function (e) {
    return next(e);
  });
}

/**
 * Delete user.
 * @returns {User}
 */
function remove(req, res, next) {
  var user = req.user;
  user.removeAsync().then(function (deletedUser) {
    var returnObj = {
      success: true,
      message: 'user deleted successfully',
      data: deletedUser
    };
    res.send(returnObj);
  }).error(function (e) {
    return next(e);
  });
}

/**
 * Load user and append to req.
 */
function load(req, res, next, id) {
  _user2.default.get(id).then(function (user) {
    req.user = user; // eslint-disable-line no-param-reassign
    return next();
  }).error(function (e) {
    return next(e);
  });
}

function hashed(password) {
  console.log(password);
  return new Promise(function (resolve, reject) {
    _bcrypt2.default.genSalt(10, function (err, salt) {
      if (err) {
        reject(err);
      }
      _bcrypt2.default.hash(password, salt, function (hashErr, hash) {
        if (hashErr) {
          reject(hashErr);
        }
        resolve(hash);
      });
    });
  });
}

function forgotReset(req, res, next) {
  var phoneNo = req.body.phoneNo;
  _user2.default.findOne({ phoneNo: req.body.phoneNo }).then(function (user) {
    if (user) {
      var password = req.body.password;
      hashed(password).then(function (newhash) {
        user.password = newhash;
        user.save().then(function (doc) {
          res.send("password Updated");
        });
      });
    } else {
      res.send('User doesnt exist');
    }
  });
}

function reset(req, res, next) {
  var userId = req.body.userId;
  // var jwtAccessToken = req.body.jwtAccessToken;
  _user2.default.get(userId).then(function (user) {
    if (user) {
      var password = req.body.password;
      hashed(password).then(function (newhash) {

        console.log(newhash);
        user.password = newhash;
        user.save().then(function (doc) {
          res.send("password Updated");
        });
      });
    } else {
      res.send('User doesnt exist');
    }
  });
}

function changepassword(req, res, next) {
  var userId = req.body.userId;
  // var jwtAccessToken = req.body.jwtAccessToken;
  _user2.default.get(userId).then(function (user) {
    if (user) {

      var oldPassword = req.body.OldPassword;
      var newPassword = req.body.newPassword;
      user.comparePassword(oldPassword, function (err, isMatch) {
        if (err) throw err;
        console.log(isMatch);
        if (isMatch === true) {
          user.password = newPassword;
          user.save().then(function (doc) {
            res.send("password Updated");
          });
        } else {
          res.send("password didn't match");
        }
      });
    } else {
      res.send("User doesnt exist");
    }
  }).error(function (e) {
    return next(e);
  });
  next();
}

function getUserDetails(req, res, next) {
  var id = req.query.jsonString;

  _user2.default.findOne({ _id: id }).then(function (doc) {

    var obj = {
      firstName: doc.fname,
      address: { addressAsText: doc.addressInfo.city },
      lastName: doc.lname,
      coverImage: { srcPath: doc.coverImage },
      profilePicture: { srcPath: doc.profilePicture },
      ownerBikeDetails: doc.bikename,
      aboutMe: doc.aboutMe,
      listofTags: doc.listofTags
    };
    console.log(obj);
    res.send(obj);
  });
}

function userInterests(req, res, next) {
  var id = req.body.userId;
  _user2.default.findOne({ _id: id }).then(function (docs) {
    var i;
    for (i = 0; i < req.body.interests.length; i++) {
      docs.listofTags.push({ userTags: req.body.interests[i] });
    }
    docs.save().then(function (user) {
      res.send("Updated User Interests");
    });
  });
}

exports.default = {
  load: load,
  get: get,
  create: create,
  update: update,
  remove: remove,
  forgot: forgot,
  changepassword: changepassword,
  reset: reset,
  getUserDetails: getUserDetails,
  userInterests: userInterests,
  forgotReset: forgotReset
};
module.exports = exports['default'];
//# sourceMappingURL=user.js.map
