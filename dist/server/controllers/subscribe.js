'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _subscribe = require('../models/subscribe');

var _subscribe2 = _interopRequireDefault(_subscribe);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function subscribed(req, res) {
	var email = req.body.email;
	var subscribe = new _subscribe2.default({
		email: email
	});

	subscribe.save().then(function (doc) {
		res.send(doc);
	}, function (e) {
		console.log(e);
	});
}

exports.default = {
	subscribed: subscribed
};
module.exports = exports['default'];
//# sourceMappingURL=subscribe.js.map
