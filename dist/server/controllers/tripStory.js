'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _tripStory = require('../models/tripStory');

var _tripStory2 = _interopRequireDefault(_tripStory);

var _multer = require('multer');

var _multer2 = _interopRequireDefault(_multer);

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

var _env = require('../../config/env');

var _env2 = _interopRequireDefault(_env);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getStory(req, res, next) {

  var storyId;
  var nextStory = {
    imagePath: null,
    title: null
  };

  var previousStory = {
    imagePath: null,
    title: null
  };

  if (req.body.requestContentJSON) {
    storyId = req.body.requestContentJSON;
  };

  //To find previos Story
  _tripStory2.default.find({ '_id': { '$lt': storyId } }, 'tripStoryImages storyTitle').sort({ '_id': -1 }).limit(1).then(function (data) {
    if (data[0].tripStoryImages.length > 0) {
      previousStory = {
        thumbnailImagePath: data[0].tripStoryImages[0].srcPath,
        title: data[0].storyTitle
      };
    }
  }, function (err) {
    console.log(err);
  });

  //To find next Story
  _tripStory2.default.find({ '_id': { '$gt': storyId } }, 'tripStoryImages storyTitle').sort('_id').limit(1).then(function (data) {
    if (data[0].tripStoryImages.length > 0) {
      nextStory = {
        thumbnailImagePath: data[0].tripStoryImages[0].srcPath,
        title: data[0].storyTitle
      };
    }
  }, function (err) {
    console.log(err);
  });

  _tripStory2.default.findOne({ _id: storyId }).populate('postedBy', 'fname lname profilePicture').then(function (doc) {
    var returnObj = {
      tripStoryId: doc._id,
      title: doc.storyTitle,
      tripStoryBody: doc.storyBody,
      summary: doc.storySummary,
      postedOn: doc.postedOn,
      coverImage: { srcPath: doc.coverImage },
      postedByUser: {
        firstName: doc.postedBy.fname,
        lastName: doc.postedBy.lname,
        profilePicture: { srcPath: doc.postedBy.profilePicture }
      },
      tripImages: doc.tripStoryImages,
      categories: { category: doc.categoryName }
    };
    returnObj.previousStory = previousStory;
    returnObj.nextStory = nextStory;
    console.log("----------------=--=-=-=-=--=-=-=-=-=--=-=-=-");
    console.log(returnObj);
    res.send(returnObj);
  }, function (err) {
    res.send(err);
  });
}

function create(req, res, next) {
  console.log("Instory Create ..");
  console.log(req);
  var imagePath, imageName;
  var arrayFilePath = [];
  var returnObj = {
    success: true,
    message: '',
    data: {}
  };

  var storage = _multer2.default.diskStorage({
    destination: function destination(req, file, cb) {
      cb(null, './node/assets/TripStory');
    },
    filename: function filename(req, file, cb) {
      imageName = Date.now() + file.originalname;
      cb(null, imageName);
      imagePath = './node/assets/TripStory' + imageName;
    }
  });

  var uploadObj = (0, _multer2.default)({ storage: storage }).array('tripImages', 10);
  uploadObj(req, res, function (err) {
    if (err) {
      returnObj.data.story = null;
      returnObj.message = 'Error Creating Story';
      returnObj.success = false;
      res.send(returnObj);
    } else {
      req.files.forEach(function (file) {
        arrayFilePath.push({ srcPath: file.path });
      });

      var story = new _tripStory2.default({
        storyTitle: req.body.storyTitle,
        storyBody: req.body.editordata,
        postedOn: Date.now(),
        storySummary: req.body.storySummary,
        postedBy: req.body.postedBy,
        storyTag: req.body.storyTag,
        categoryName: req.body.storyCategory,
        tripStoryImages: arrayFilePath
      });
      story.saveAsync().then(function (savedStory) {
        var username = 'admin';
        var password = 'admin';
        var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
        _request2.default.post({
          headers: {
            'content-type': 'application/x-www-form-urlencoded',
            "Authorization": auth
          },
          url: _env2.default.aemAuthorUrl + '/bin/createPage',
          form: {
            entity: 'trip-story',
            pageId: savedStory._id.toString(),
            title: savedStory.storyTitle,
            category: savedStory.categoryName,
            pageProperties: JSON.stringify({
              "title": req.body.storyTitle,
              "author": req.body.postedByUserName,
              "category": req.body.storyCategory,
              "summary": req.body.storySummary,
              "tags": req.body.storyTag,
              "thumbnailImagePath": arrayFilePath[0].srcPath,
              "postedBy": req.body.postedBy,
              "thumbnailImagePathAltText": req.body.storyTitle
            })
          }
        }, function (error, response, data) {
          var data = JSON.parse(data);
          if (error) {
            returnObj.data.story = null;
            returnObj.message = 'Error Creating Story';
            returnObj.success = false;
            res.send(returnObj);
          } else {
            _tripStory2.default.findOneAndUpdate({ _id: savedStory._id }, { $set: { storyUrl: data.pagePath } }).then(function (updatedStory) {
              returnObj.data.story = savedStory;
              returnObj.data.story.storyUrl = data.pagePath;
              returnObj.message = 'story created successfully';
              res.send(returnObj);
            });
          }
        });
      }).error(function (e) {
        return next(e);
      });
    }
  });
}

function update(req, res, next) {
  _tripStory2.default.findOneAndUpdate({ _id: req.body._id }, {
    $set: {
      storyTitle: req.body.storyTitle,
      storyBody: req.body.storyBody,
      tripImages: req.body.tripImages,
      storyUrl: req.body.storyUrl
    }
  }).then(function (savedStory) {
    var returnObj = {
      success: true,
      message: 'Story updated successfully',
      data: savedStory
    };
    res.send(returnObj);
  }).error(function (e) {
    return next(e);
  });
}

function remove(req, res, next) {
  // code to delete story goes here...
  _tripStory2.default.find({ _id: req.body._id }).then(function (doc) {
    if (doc[0].postedBy == req.body.userid) {
      doc.remove();
    } else {
      res.send("invalid user");
    }
  });
}

function getStories(req, res, next) {
  _tripStory2.default.getStories(parseInt(req.body.skip), parseInt(req.body.limit)).then(function (stories) {
    var returnObj = {
      success: true,
      message: '',
      data: {}
    };
    returnObj.data.stories = stories;
    returnObj.message = 'stories retrieved successfully';
    res.send(returnObj);
  }).error(function (e) {
    return next(e);
  });
}

function getmystory(req, res, next) {
  var id = req.body.tripstoryid;
  _tripStory2.default.find({ _id: id }).populate({
    path: 'comment',
    model: 'comments',
    populate: {
      path: 'Replyid userdetailscomments',
      populate: { path: 'userdetailsreplies' }
    }
  }).then(function (tripstory) {
    res.send(tripstory[0]);
  });
}

exports.default = {
  getStories: getStories,
  getStory: getStory,
  create: create,
  update: update,
  remove: remove,
  getmystory: getmystory
};
module.exports = exports['default'];
//# sourceMappingURL=tripStory.js.map
