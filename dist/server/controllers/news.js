'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _news = require('../models/news');

var _news2 = _interopRequireDefault(_news);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getNews(req, res, next) {
    var id = req.body.newsUrl;

    _news2.default.findOne({ _id: id }).populate({
        path: 'comment',
        model: 'comments',
        populate: {
            path: 'Replyid userdetailscomments',
            populate: { path: 'userdetailsreplies' }
        }
    }).then(function (news) {
        res.send(news);
    });
}

function create(req, res, next) {
    var news = new _news2.default({
        newsUrl: req.body.newsUrl
    });
    news.saveAsync().then(function (savedNews) {
        res.send(savedNews._id);
    }).error(function (e) {
        return next(e);
    });
}

function update(req, res, next) {
    _news2.default.findOne({ _id: req.body._id }).then(function (doc) {
        doc.comment = req.body.comment ? req.body.comment : doc.comment;
    });
    _news2.default.saveAsync().then(function (updatedNews) {
        res.send(updatedNews + "updated Successfully");
    });
}

exports.default = {
    create: create,
    getNews: getNews,
    update: update
};
module.exports = exports['default'];
//# sourceMappingURL=news.js.map
