'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _user = require('../models/user');

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function mobileVerify(req, res, next) {
  _user2.default.findOneAsync({ email: req.query.email })
  //eslint-disable-next-line
  .then(function (foundUser) {
    if (foundUser) {
      var host = req.get('host');
      if (req.protocol + "://" + req.get('host') == "http://" + host) {
        if (req.query.otp == foundUser.otp) {
          _user2.default.findOneAndUpdateAsync({ email: req.query.email }, { $set: { mobileVerified: true } }, { new: true }) //eslint-disable-line
          .then(function (updateUserObj) {
            //eslint-disable-line
            if (updateUserObj) {
              var returnObj = {
                success: false,
                message: 'Mobile verified',
                data: {}
              };
              returnObj.success = true;
              return res.send(returnObj);
            }
          }).error(function (e) {
            var err = new APIError('error in updating user details while login ' + e, httpStatus.INTERNAL_SERVER_ERROR);
            next(err);
          });
        } else {
          var returnObj = {
            success: false,
            message: 'Error Verifying Mobile',
            data: {}
          };
          return res.send(returnObj);
        }
      }
    }
  });
} /* eslint-disable */


function emailVerify(req, res, next) {
  _user2.default.findOneAsync({ email: req.query.email })
  //eslint-disable-next-line
  .then(function (foundUser) {
    if (foundUser) {
      var host = req.get('host');
      if (req.protocol + "://" + req.get('host') == "http://" + host) {
        if (req.query.verificationToken == foundUser.emailToken) {
          _user2.default.findOneAndUpdateAsync({ email: req.query.email }, { $set: { emailVerified: true } }, { new: true }) //eslint-disable-line
          .then(function (updateUserObj) {
            //eslint-disable-line
            if (updateUserObj) {
              var returnObj = {
                success: false,
                message: 'Email verified',
                data: {}
              };
              returnObj.success = true;
              return res.send(returnObj);
            }
          }).error(function (e) {
            var err = new APIError('error in updating user details while login ' + e, httpStatus.INTERNAL_SERVER_ERROR);
            next(err);
          });
        } else {
          var returnObj = {
            success: false,
            message: 'Error Verifying Email',
            data: {}
          };
          return res.send(returnObj);
        }
      }
    }
  });
}

function verifyOtp(req, res, next) {
  var phoneNo = req.body.phoneNo;
  _user2.default.find({ phoneNo: phoneNo }).then(function (user) {
    var otp = req.body.otp;
    console.log(otp);
    if (user[0].otp == otp) {
      res.send("Otp verified");
    } else {
      res.send("Otp wrong");
    }
  }).catch(function (e) {
    console.log(e);
  });
}

exports.default = { emailVerify: emailVerify, mobileVerify: mobileVerify, verifyOtp: verifyOtp };
module.exports = exports['default'];
//# sourceMappingURL=verify.js.map
