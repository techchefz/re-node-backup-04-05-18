'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _nodemailer = require('nodemailer');

var _nodemailer2 = _interopRequireDefault(_nodemailer);

var _nodemailerSmtpTransport = require('nodemailer-smtp-transport');

var _nodemailerSmtpTransport2 = _interopRequireDefault(_nodemailerSmtpTransport);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _user = require('../models/user');

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint-disable */
var EmailTemplate = require('email-templates').EmailTemplate;

var emailDir = _path2.default.resolve(__dirname, '../templates', 'emailVerify');
var emailVerify = new EmailTemplate(_path2.default.join(emailDir));

var registerDir = _path2.default.resolve(__dirname, '../templates', 'register');
var register = new EmailTemplate(_path2.default.join(registerDir));

function sendEmail(userId, responseObj, type) {
  _user2.default.findOneAsync({ _id: userId }).then(function (userObj) {
    var details = {
      host: "smtp.mailtrap.io",
      port: 2525,
      username: "74b65bc36e217c",
      password: "453fbd4ef2ce51"
    };
    var transporter = _nodemailer2.default.createTransport((0, _nodemailerSmtpTransport2.default)({
      host: details.host,
      port: details.port,
      secure: details.secure, // secure:true for port 465, secure:false for port 587
      auth: {
        user: details.username,
        pass: details.password
      }
    }));
    var locals = Object.assign({}, { data: responseObj });
    if (type === 'emailVerify') {
      emailVerify.render(locals, function (err, results) {
        //eslint-disable-line
        if (err) {
          return err; //eslint-disable-line
        }
        var mailOptions = {
          from: details.username, // sender address
          to: userObj.email, // list of receivers
          subject: 'Verify your Account with RoyalEnfield', // Subject line
          text: results.text, // plain text body
          html: results.html // html body
        };
        transporter.sendMail(mailOptions, function (error, info) {
          if (error) {
            return error;
          }
          return info;
        });
      });
    }

    if (type === 'register') {
      register.render(locals, function (err, results) {
        //eslint-disable-line
        if (err) {
          return err; //eslint-disable-line
        }
        var mailOptions = {
          from: details.username, // sender address
          to: userObj.email, // list of receivers
          subject: 'Your Account with RoyalEnfield is created', // Subject line
          text: results.text, // plain text body
          html: results.html // html body
        };
        transporter.sendMail(mailOptions, function (error, info) {
          if (error) {
            return error;
          }
          return info;
        });
      });
    }

    if (type === 'forgot') {
      forgot.render(locals, function (err, results) {
        if (err) {
          return err;
        }
        var mailOptions = {
          from: details.username, // sender address
          to: userObj.email, // list of receivers
          subject: 'Your Account Password for RoyalEnfield', // Subject line
          text: results.text, // plain text body
          html: results.html // html body
        };
        transporter.sendMail(mailOptions, function (error, info) {
          if (error) {
            return error;
          }
          return info;
        });
      });
    }
  });
}
exports.default = sendEmail;
module.exports = exports['default'];
//# sourceMappingURL=emailApi.js.map
