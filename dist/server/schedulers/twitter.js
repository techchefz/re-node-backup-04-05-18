'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
var Twit = require('twitter');
var util = require('util');

var _require = require('../controllers/tweetit'),
    tweetit = _require.tweetit;

var mongoose = require('mongoose');

var _require2 = require('../models/twitterfeeds'),
    Twitterfeed = _require2.Twitterfeed;

function twitterscheduler() {
	setInterval(twitterservice, 1000 * 10);
	function twitterservice() {
		Twitterfeed.find().then(function (doc) {
			if (doc.length != 0) {
				Twitterfeed.remove().then(function (docs) {
					console.log('Data deleted and now it will be updated');
				});
			}
			tweetit();
		});
	}
}
exports.default = { twitterscheduler: twitterscheduler };
module.exports = exports['default'];
//# sourceMappingURL=twitter.js.map
