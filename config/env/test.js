export default {
  aemAuthorUrl:'http://10.162.134.73:4502/',
  aemPublish:'http://10.162.134.74:4503/',
  dmsApiUrl:'http://refineb2cdvp.excelloncloud.com/exapis//api/portalentity',
  solr:'10.162.134.75',
  varnish:'http://10.162.134.75/',
  env: 'test',
  jwtSecret: '0a6b944d-d2fb-46fc-a85e-0295c986cd9f',
  db: 'mongodb://localhost/re-node-db',
  port: 5066,
  passportOptions: {
    session: false
  }
};
