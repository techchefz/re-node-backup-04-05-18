import Joi from 'joi';

export default {
  // POST /api/users/register
  createUser: {
    body: {
      email: Joi.string().required(),
      password: Joi.string().required(),
      phoneNo: Joi.string().required()
    }
  },

  // POST /api/auth/login
  login: {
    body: {
      email: Joi.string().required(),
      password: Joi.string().required(),
      userType: Joi.string().required()
    }
  },

  // POST /api/bikes/register
  createBike: {
    body: {
      bikeModel: Joi.string().required(),
      bikeName: Joi.string().required(),
      bikeCategory: Joi.string().required()
    }
  },

  // POST /api/stories/create
  createTripStory: {
    body: {
      storyTitle: Joi.string().required(),
    }
  },

  // POST /api/stories/all
  getStories: {
    body: {
      limit: Joi.string().required()
    }
  },

  // POST /api/stories/
  getStory: {
    body: {
    }
  },

  // POST /api/rides/create
  createRide: {
    body: {
      rideName: Joi.string().required()
    }
  },

  // POST /api/rides/all
  getRides: {
    body: {
      limit: Joi.string().required()
    }
  },

  // POST /api/rides/
  getRide: {
    body: {
      rideId: Joi.string().required()
    }
  },

  sendOtp: {
    body: {
      phoneNo: Joi.string().required()
    }
  },


  getBikeDetail: {
    body: {
      phoneNo: Joi.string().required()
    }
  },

  findState: {
    body: {
      state: Joi.string().required()
    }
  },

  findCity: {
    body: {
      city: Joi.string().required()
    }
  },
};
